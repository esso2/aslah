<?php

use App\Http\Controllers\API\AboutusController;
use App\Http\Controllers\API\AdvertisersController;
use App\Http\Controllers\API\ProbagandaController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\AuthOwnerController;
use App\Http\Controllers\API\BannersController;
use App\Http\Controllers\API\CartController;
use App\Http\Controllers\API\CitiesController;
use App\Http\Controllers\API\ComplaintController;
use App\Http\Controllers\API\HelpingController;
use App\Http\Controllers\API\NewChatController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\OwnerController;
use App\Http\Controllers\API\OwnerServicesController;
use App\Http\Controllers\API\RateController;
use App\Http\Controllers\API\SearchController;
use App\Http\Controllers\API\ServicesController;
use App\Http\Controllers\API\TermsController;
use App\Http\Controllers\API\UserOrdersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
--------------------------------------------------------------------------
API Routes
--------------------------------------------------------------------------
*/
    //register 
    Route::post('/register', [AuthController::class,'register']);
    //verify_code
    Route::post('/verify', [AuthController::class,'verify'])->name('verify');
    //reset_code
    Route::post('/reset', [AuthController::class,'resetCode']);
    //update password
    Route::post('/updatepassword', [AuthController::class,'updatePassword']);
    //banners
    Route::get('banners',[BannersController::class,'index']);
    //cities
    Route::get('cities',[CitiesController::class,'index']);
    //advertisers
    Route::get('advertisers',[AdvertisersController::class,'index']);
    //owners
    Route::get('owners',[OwnerController::class,'index']);
    //owner_details
    Route::post('ownerdetails', [OwnerController::class,'ownerDetails']);
    //login
    Route::post('/login', [AuthController::class,'login']);
    //forget password
    Route::post('/forgetpassword', [AuthController::class,'forgetPassword'])->name('forgetpassword');
    //complaints
    Route::post('storecomplaints',[ComplaintController::class,'storecomp']);
    //aboutus
    Route::get('aboutus',[AboutusController::class,'aboutus']);
    //terms and conditions
    Route::get('terms',[TermsController::class,'index']);
    //main_services
    Route::get('mainservices',[ServicesController::class,'index']);

######################### Group of Routes Users ###########################
Route::group(['middleware' => 'auth:api'], function() {

    //logout
    Route::get('logout', [AuthController::class,'logout']);
    //helping
    Route::get('helping',[HelpingController::class,'index']);
    //probaganda
    Route::get('probaganda',[ProbagandaController::class,'index']);
    //user_details
    Route::get('user', [AuthController::class,'user']);
    //rate
    Route::post('rate',[RateController::class,'rateOwner']);
    //user_orders
    Route::get('orders',[OrderController::class,'orders']);
    //owner_name_search
    Route::post('search', [SearchController::class,'search']);
    //add_to_cart
    Route::post('add-to-cart',[OrderController::class,'addOrder']);
    //user_update
    Route::post('/user/update/{id}',[AuthController::class,'updateUser']);
    //current orders
    Route::get('current_orders',[UserOrdersController::class,'currentOrder']);
    //finished orders
    Route::get('finished_orders',[UserOrdersController::class,'finishedOrder']);
    //order details
    Route::post('order_details',[UserOrdersController::class,'orderDetails']);
    //users
    Route::apiResource('/users',UserController::class);
    //sub_list
    Route::post('sublist',[ServicesController::class,'subList']);
    //end_list
    Route::post('endlist',[ServicesController::class,'endList']);
    // chats
    Route::post('makechat',[NewChatController::class,'makechat']);
    Route::post('getchat',[NewChatController::class,'getchat']);
    Route::post('getchaters',[NewChatController::class,'getchaters']);
    Route::post('delchat',[NewChatController::class,'delchat']);


});


