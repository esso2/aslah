<?php

use App\Http\Controllers\API\AboutusController;
use App\Http\Controllers\API\AdvertisersController;
use App\Http\Controllers\API\ProbagandaController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\AuthOwnerController;
use App\Http\Controllers\API\BannersController;
use App\Http\Controllers\API\CartController;
use App\Http\Controllers\API\CitiesController;
use App\Http\Controllers\API\ComplaintController;
use App\Http\Controllers\API\HelpingController;
use App\Http\Controllers\API\NewChatController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\OwnerController;
use App\Http\Controllers\API\OwnerOrdersController;
use App\Http\Controllers\API\OwnerServicesController;
use App\Http\Controllers\API\RateController;
use App\Http\Controllers\API\SearchController;
use App\Http\Controllers\API\ServicesController;
use App\Http\Controllers\API\TermsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


    Route::post('/registerowner', [AuthOwnerController::class,'register']);
    //verify_code_owner
    Route::post('/verifyowner', [AuthOwnerController::class,'verify']);
    //reset_owner_code
    Route::post('/resetownerpassword', [AuthOwnerController::class,'resetCode']);
    //update password
    Route::post('/updateownerpassword', [AuthOwnerController::class,'updatePassword']);
    //login
    Route::post('/loginowner', [AuthOwnerController::class,'login']);

Route::group(['middleware' => ['auth:owner']], function() {


    //owner_update
    Route::post('/owner/update/{id}',[AuthOwnerController::class,'updateOwner']);
    //owner_details
    Route::get('ownerdetails', [AuthOwnerController::class,'ownerDetails']);
    //owner_main_service
    Route::get('ownermainservice', [OwnerServicesController::class,'ownerMainService']);
    //owner_service_details
    Route::get('ownerservicedetails', [OwnerServicesController::class,'OwnerServiceDetails']);
    //sub_services_list
    Route::post('subserviceslist',[ServicesController::class,'subServicesList']);
    //end_services_list
    Route::post('endserviceslist',[ServicesController::class,'endServicesList']);
    //add_new_end_services
    Route::post('addendserviceslist',[ServicesController::class,'addEndServicesList']);
    //owner_services
    Route::get('owner_services',[ServicesController::class,'ownerServices']);
    //delete_end_services
    Route::post('deleteendserviceslist/{id}',[ServicesController::class,'deleteEndServicesList']);
    //current owner orders
    Route::get('current-owner-orders',[OwnerOrdersController::class,'currentOrder']);
    //finished owner orders
    Route::get('finished-owner-orders',[OwnerOrdersController::class,'finishedOrder']);
    //order details
    Route::post('order_details_owner',[OwnerOrdersController::class,'ownerOrderDetails']);
    //accept order
    Route::post('accept_order',[OwnerOrdersController::class,'acceptOrder']);
    //refuse order
    Route::post('refuse_order',[OwnerOrdersController::class,'refuseOrder']);
    //advertise list
    Route::get('advertlist',[AdvertisersController::class,'advertList']);

    //logout
    Route::get('logoutowner', [AuthOwnerController::class,'logout']);

    Route::post('makechat',[NewChatController::class,'makechat']);

    Route::post('getchat',[NewChatController::class,'getchat']);

    Route::post('getchaters',[NewChatController::class,'getchaters']);

    Route::post('delchat',[NewChatController::class,'delchat']);

});