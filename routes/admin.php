<?php

use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminsController;
use App\Http\Controllers\Admin\AdvertisingpackagesController;
use App\Http\Controllers\Admin\AdvertismentController;
use App\Http\Controllers\Admin\BannersController;
use App\Http\Controllers\Admin\ComplaintController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\HelpController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\CitiesController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\OwnerController;
use App\Http\Controllers\Admin\PropagandaController;
use App\Http\Controllers\Admin\TermsController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;



Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 
        Route::get('admin/home', [AdminController::class, 'index'])->name('adminhome');
        Route::GET('admin', [LoginController::class, 'showLoginForm'])->name('admin.login');
        Route::POST('admin',[LoginController::class, 'login']);
        Route::POST('logout',[LoginController::class, 'logout'])->name('admin.logout');

        Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {


            Route::get('complaints', [ComplaintController::class, 'index'])->name('complaints');
            Route::delete('complaints/{id}', [ComplaintController::class, 'destroy'])->name('destroycomplaints');

            Route::resource('roles', RoleController::class)->except(['show']);

            Route::resource('services', ServicesController::class)->except(['show']);
            Route::get('serviceactive', [ServicesController::class, 'ServiceStatus'])->name('serviceactive');

            Route::resource('banners', BannersController::class)->except(['show']);
            Route::get('banneractive', [BannersController::class, 'BannerStatus'])->name('banneractive');

            Route::resource('advertisments', AdvertismentController::class)->except(['show']);
            Route::get('advertismentactive', [AdvertismentController::class, 'advertismentStatus'])->name('advertismentactive');

            Route::resource('admins', AdminsController::class)->except(['show']);
            Route::get('adminactive', [AdminsController::class, 'AdminStatus'])->name('adminactive');

            Route::resource('cities', CitiesController::class)->except(['show']);
            Route::get('cityactive', [CitiesController::class, 'CityStatus'])->name('cityactive');
            
            Route::resource('packages', AdvertisingpackagesController::class)->except(['show']);
            Route::get('packageactive', [AdvertisingpackagesController::class, 'packageStatus'])->name('packageactive');

            Route::get('helps', [HelpController::class, 'index'])->name('helping');
            Route::post('helps', [HelpController::class, 'update'])->name('updatehelping');

            Route::get('terms', [TermsController::class, 'index'])->name('terming');
            Route::post('terms', [TermsController::class, 'update'])->name('updateterming');

            Route::get('propagandas', [PropagandaController::class, 'index'])->name('propaganding');
            Route::post('propagandas', [PropagandaController::class, 'update'])->name('updatepropaganding');

            Route::resource('users', UsersController::class)->except(['show']);
            Route::get('useractive', [UsersController::class, 'UserStatus'])->name('useractive');
            Route::get('useraddress', [UsersController::class, 'UserAddress'])->name('useraddress');

            Route::resource('owners', OwnerController::class);
            Route::get('owneractive', [OwnerController::class, 'OwnerStatus'])->name('owneractive');
           // Route::resource('balance/{id}', [BalanceOwnerController::class, 'destroy']);
            Route::get('ownersbalance/{id}',[OwnerController::class,'OwnersBalance'])->name('owners.balances');


            Route::resource('contactus', ContactController::class)->except(['create','edit']);


            Route::get('about', [AboutController::class, 'index'])->name('about');
            Route::post('about', [AboutController::class, 'update'])->name('updateabout');

        });  

    });

