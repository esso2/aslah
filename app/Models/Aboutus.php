<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aboutus extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'aboutuses';
    public $translatedAttributes = ['aboutuses'];
}
