<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Help extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'helps';
    
    public $translatedAttributes = ['question'];

    protected $fillable = [
        'support_number','vedio_link'
    ];
    

}
