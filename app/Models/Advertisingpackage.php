<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advertisingpackage extends Model
{
    use HasFactory;
    
    protected $fillable = ['period','price','active'];

    public function owners(){
        return $this->belongsToMany(Advertisment::class,'package_id','owner_id');
    }
}
