<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Service extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'services';

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'parent_id',
        'image',
        'active',
    ];
    
    
    protected $appends = ['image_path'];

    
    public function getImagePathAttribute()
    {
        return asset($this->image);
    }
    
    public function owners()
    {
        return $this->hasMany(Owner::class);
    }

    public function servicesowners(){
        return $this->belongsToMany(Owner::class,'owner_services')->withPivot('price','description')->withTimestamps();
    }

    public function service()
    {
        return $this->belongsTo(Service::class,'parent_id');
    }
    
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
}
