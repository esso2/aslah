<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaints extends Model
{
    use HasFactory;
    protected $fillable = ['type','name','phone','message','image','order_id'];


    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        return asset( $this->image);
    }

    
    // public function orders()
    // {
    //     return $this->belongsTo(Order::class);
    // }

}
