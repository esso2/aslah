<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Propaganda extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'propagandas';
    public $translatedAttributes = ['propagandas'];
}
