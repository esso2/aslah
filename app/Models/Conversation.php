<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use HasFactory;

    protected $fillable = ['sender_id','reciever_id'];

    
    public function sender(){
        return $this->belongsTo(Owner::class,'owner_id');
    }

    public function receiver(){
        return $this->belongsTo(User::class,'user_id');
    }
    
    public function chats(){
        return $this->hasMany(Chat::class)->orderBy('created_at','desc');
    }
    

}
