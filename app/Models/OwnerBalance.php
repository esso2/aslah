<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerBalance extends Model
{
    use HasFactory;
    protected $fillable = ['type','balance','owner_id'];


    public function owners(){

        return $this->belongsTo(Owner::class);

    }
}
