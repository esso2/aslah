<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Owner extends Authenticatable
{
    use HasApiTokens ,HasFactory,Notifiable;
    
    protected $fillable = [
        'name',
        // 'manager_name',
        'email',
        'password',
        'phone',
        // 'manager_phone',
        'commerical',
        // 'image_commerical',
        'service_id',
        'city_id',
        'type',
        'from',
        'to',
        'address',
        'code',
        'isVerified',
        'google_token',
        'min_salary',
        'bank_name',
        'bank_account_owner',
        'account_number',
        // 'national_number',
        'image',
        'image_owner',
        'location',
        'active',
    ];
    
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    protected $appends = ['image_path'];

    

        public function getImagePathAttribute(){
            return asset($this->image);
        }

        public function getImageOwnerPathAttribute(){
            return asset($this->image_owner);
        }

        public function services(){
            return $this->belongsTo(Service::class,'service_id');
        }

        public function ownerservices(){
            return $this->belongsToMany(Service::class,'owner_services')->withPivot('price','description')->withTimestamps();
        }

        public function cities(){
            return $this->belongsTo(City::class,'city_id');
        }

        public function balances()
        {
            return $this->hasOne(OwnerBalance::class);
        }

        public function packages()
        {
            return $this->belongsToMany(Advertisment::class,'package_id','owner_id');
        }

        public function rates()
        {
            return $this->hasMany(Rate::class);
        }

        public function orders()
        {
            return $this->hasMany(Order::class);
        }
}
