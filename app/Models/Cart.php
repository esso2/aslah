<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['price','notes','image','total_price','quantity','service_id','user_id','lat','lang','address'];

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        return asset('uploads/cart/' . $this->image);
    }

    public function services(){
        return $this->hasMany(Service::class);
    }

    public function users(){
        return $this->belongsTo(User::class);
    }

    // public function userAddresses(){
    //     return $this->hasOne(Address::class);
    // }
}
