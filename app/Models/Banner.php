<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'image',
        'banner_url'
    ];

    protected $appends = ['image_path'];

    
    public function getImagePathAttribute()
    {
        return asset($this->image);
    }

}
