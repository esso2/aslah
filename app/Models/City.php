<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class City extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'cities';

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'active',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class,'user_addresses','city_id','user_id');
    }

    // public function owners()
    // {
    //     return $this->belongsToMany(User::class,'owner_cities','city_id','owner_id');
    // }

    public function owners()
    {
        return $this->hasMany(Owner::class);
    }


}
