<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','owner_id','rate'];

    public function owners()
    {
        return $this->belongsTo(Owner::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

}
