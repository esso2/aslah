<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
    'user_id',
    'owner_id',
    'service_id',
    'action',
    'refuse_reason',
    'total_price',
    'price',
    'quantity',
    'lat',
    'lang',
    'address',
    'note',
    'res_time',
    'res_date',
    'image',
];

protected $appends = ['image_path'];

    

public function getImagePathAttribute(){
    return asset($this->image);
}

 public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
 public function owners()
    {
        return $this->belongsTo(Owner::class,'owner_id');
    }

 public function services()
    {
        return $this->belongsTo(Service::class,'service_id');
    }


}
