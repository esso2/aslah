<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $fillable = [
        'sender_id',
        'reciever_id',
        'conversation_id',
        'message','file',
        'type'
    ];
    
    public function conversation(){
        return $this->belongsTo(Conversation::class,'conversation_id');
    }

    public function sender(){
        return $this->belongsTo(Owner::class,'owner_id');
    }

    public function receiver(){
        return $this->belongsTo(User::class,'user_id');
}
