<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class OwnerService extends Model
{
    use HasFactory;

    protected $table = 'owner_services';

    protected $fillable = ['service_id','owner_id','price','description'];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];


}
