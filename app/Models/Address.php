<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Address extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'addresses';

    public $translatedAttributes = ['nameaddress'];

    protected $fillable = [
        'user_id',
        'city_id',
        'location',
    ];

    
    public function users(){
        return $this->belongsTo(User::class);
    }

    // public function cart(){
    //     return $this->belongsTo(Cart::class);
    // }

}
