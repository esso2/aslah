<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class OrderController extends Controller
{

    public function addOrder(Request $request){ 

        $all_orders = $request->user_orders;
        $all_orders=json_decode($all_orders);
        
        foreach ($all_orders as $item) {
            $orders = new Order();
            $orders->user_id = auth()->user()->id;
            $orders->service_id = $item->service_id;
            $orders->owner_id = $item->owner_id;
            $orders->price = $item->price;
            $orders->quantity = $item->quantity;
            $orders->note = $item->note;
            $orders->total_price = $item->price * $item->quantity;
            $orders->res_date  = $request->input('res_date');
            $orders->res_time  = $request->input('res_time');
            $orders->address  = $request->input('address');
            $orders->lang  = $request->input('lang');
            $orders->lat  =$request->input('lat');
            
            // if ($item->image) {
            //     Image::make($item->image)->resize(300, null, function ($constraint) {
            //         $constraint->aspectRatio();
            //     })->save(public_path('uploads/orders/' . $item->image->hashName()));
    
            //     $orders->image ='uploads/orders/' .  $item->image->hashName();
            // }
    
            $orders->save();

            // $not = new Notification();
            // $not->title = "لديك اشعار جديد"; 
            // $not->body = "لقد تلقيت جحز جديد";
            // $not->title_en= "new notification"; 
            // $not->body_en = "you have new order";
            // $not->user_id = $item->owner_id;
            // $not->save();

            // $to = User::where([['id',$item->owner_id]])->value('google_token');

            // $message = [
            //     "owner_id"=>$item->owner_id,
            //     "sender_name"=>$item->user_id
            //     ];

            // $fields = array(
            // 'to'   => $to,
            // "notification"=>[
            //     "title"=>"لديك اشعار جديد ",
            //     "body"=>" لقد تلقيت حجز جديد  "
            //     ],
            // 'data'=>$message
            // );

            // // Set POST variables
            // $url = 'https://fcm.googleapis.com/fcm/send';
            // $ch = curl_init();

            // // Set the url, number of POST vars, POST data
            // curl_setopt( $ch, CURLOPT_URL, $url );
            // curl_setopt( $ch, CURLOPT_POST, true );
            // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; ',
            // 'Authorization: key = '));
            // curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            // // Disabling SSL Certificate support temporarly
            // curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            // curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            // $result = curl_exec( $ch );
            // if ( $result === false ) {
            // die( 'Curl failed: ' . curl_error( $ch ) );
            // }
            // curl_close( $ch );

        }

            return response()->json([
                'status'=>'success',
                'message'=>'order Added Successfully',
            ],200);
    }
}
