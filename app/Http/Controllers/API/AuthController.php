<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;


//use Twilio\Rest\Client;

class AuthController extends Controller
{

    // Registerion function

    public function register(Request $request)
    {

        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'phone' => 'required|string|unique:users',
            'password' => 'required|string|min:3',
            'confirm_password' => 'required|string|min:3',
            'google_token' => 'required',
        ]);

        //    $token = env('TWILIO_AUTH_TOKEN');
        //    $twilio_sid = env('TWILIO_SID');
        //    $twilio_verify_sid = env('TWILIO_VERIFY_SID');
        //    $twilio = new Client($twilio_sid, $token);
        //    $twilio->verify->v2->services($twilio_verify_sid)
        //        ->verifications
        //        ->create($data['phone'], "sms");

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'code' => mt_rand(1000, 9999),
            'google_token' => $request->google_token,
        ]);
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        if ($user == true) {

        return response()->json([
            'status' => 'success',
            'message' => 'Must verfiy your account!',
            'user_id' =>  $user->id,
            'type' => 'user',
            'phone' => $data['phone'],
            'google_token' => $data['google_token'],
            'code' => $user->code,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
            ], 201);

            }else{

                return response()->json([
                    'status' => 'Error',
                    'message' => 'You are Not Register'
                ]);
            }
    }


    //    protected function verify(Request $request)
    //    {
    //        $data = $request->validate([
    //            'verification_code' => ['required', 'numeric'],
    //            'phone' => ['required', 'string'],
    //        ]);
    //     //    /* Get credentials from .env */
    //        $token = getenv("TWILIO_AUTH_TOKEN");
    //        $twilio_sid = getenv("TWILIO_SID");
    //        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
    //        $twilio = new Client($twilio_sid, $token);
    //        $verification = $twilio->verify->v2->services($twilio_verify_sid)
    //            ->verificationChecks
    //            ->create($data['verification_code'], array('to' => $data['phone']));

    //        if ($verification->valid) {
    //            $user = tap(User::where('phone', $data['phone']))->update(['isVerified' => true]);
    //            /* Authenticate user */
    //            Auth::login($user->first());

    //            return response()->json([
    //                'status' => 'Success',
    //                'message' => 'Phone number verified!'
    //            ], 201);
    //        }

    //        return response()->json([
    //            'status' => 'Error',
    //            'message' => 'Invalid verification code entered'
    //        ]);
    //    }

    public function verify(Request $request)
    {

        $data = $request->validate([
            'code' => ['required', 'numeric'],
            'user_id' => ['required', 'string'],
        ]);

          $user = tap(User::where('id', $data['user_id'])->where('code', $data['code']))->update(['isVerified' => true]);

           if ($user == true) {
            /* Authenticate user */

            Auth::login($user->first());

            return response()->json([
                'status' => 'Success',
                'message' => 'Phone number verified!'
            ], 201);

            } else {

            return response()->json([
                'status' => 'Error',
                'message' => 'Invalid verification code entered'
            ]);
        }
    }

    // resnt code verify

    public function resetCode(Request $request)
    {
      $user = User::where('phone', $request->phone)->first();
      //dd($user);
       if($user){ 
        return response()->json([
            'status' => 'success',
            'user_id' =>  $user->id,
            'phone' => $user->phone,
            'code' => $user->code
        ], 200);
        }
    }

        // update password

        public function UpdatePassword(Request $request)
        {
            $data = $request->validate([
                'phone' => 'required|string|min:3',
                'password' => 'required|string|min:3',
                'confirm_password' => 'required|string|min:3',
            ]);

            $user = User::where('phone', $data['phone'])->first();

            $user->update([
                'password' =>Hash::make($data['password']),
            ]);

            $user->save();

        if($user){ 
            return response()->json([
                'status' => 'success',
                'message' => 'Password Updated'
            ], 200);
            }
        }

    // Login functions

    public function login(Request $request)
    {

        $request->validate([
            'phone' => 'required', 'string',
            'password' => 'required|string',
            // 'remember_me' => 'boolean'
        ]);

        $credentials = request(['phone', 'password']);

        if (!Auth::attempt($credentials)){

            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized'
            ], 401);

        }elseif (auth()->user()->isVerified == false){

            return response()->json([
                'message' => 'not verfiy'
            ], 404);

        }elseif (auth()->user()->isVerified == true){

            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();

            return response()->json([
            'status'=>'success', 
            'message' => 'you are loged in',
            'user_id' => auth()->user()->id,
            'type' => 'user',
            'access_token' => $tokenResult->accessToken,
            'google_token' => $user->google_token,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

            ], 200);
        }
    }


    // Logout Function

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }



    // Profile data

    public function user()
    {
        // auth('api')->user()
        return response()->json(auth()->user());
    }


    public function updateUser(UpdateUser $request,$id){

        //dd($request->all());
        // $user = User::findOrFail($id);
        //     $user->name      = $request->name;
        //     $user->phone     = $request->phone;
        //     $user->email     = $request->email;
        //     $user->password  = Hash::make($request->password);

        // if ($request->image) {
        //     Image::make($request->image)->resize(300, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     })->save(public_path('uploads/users/' . $request->image->hashName()));
        //     $user->image ='uploads/users/' . $request->image->hashName();
        // }
        // $user->save();
        // return response()->json([
        //     'status'=>'success',
        //     'message' => 'Successfully Updated'
        // ],200);

            //dd($request->all());
            $user = User::findOrFail($id);
            $user->update([
                'name'      => $request->name,
                'phone'     => $request->phone,
                'email'     => $request->email,
                'password'  => Hash::make($request->password),
            ]);
            

            if ($request->image) {
                Image::make($request->image)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('uploads/users/' . $request->image->hashName()));
                $user->image ='uploads/users/' . $request->image->hashName();
            }

            $user->save();
            return response()->json([
                'status'=>'success',
                'message' => 'Successfully Updated',
                'data' =>$user
            ],200);

    }

    

}
