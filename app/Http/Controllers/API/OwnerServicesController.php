<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Owner;
use Illuminate\Http\Request;

class OwnerServicesController extends Controller
{
    public function ownerMainService(){
        $owner = auth()->user();
        // dd($owner->service_id);
        $mainservice = Owner::where('owners.id', $owner->id)->first();
        // dd($mainservice);
        return response()->json([ 
            'status'=>'success',
            'owner_main_service' =>  [
                'id' => $mainservice->services->id,
                'image' => $mainservice->services->image,
                'name_ar' => $mainservice->services->translate('ar')->name,
                'name_en' => $mainservice->services->translate('en')->name,
            ]
        ],200);
    }

    public function OwnerServiceDetails(){

        $owner = auth()->user();

        $servicedetail = Owner::where('owners.id', $owner->id)->first();

        return response()->json([ 
            'status'=>'success',
            'services_details'=>$servicedetail->services,
        ],200);

    }

}
