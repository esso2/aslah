<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdvertiserResource as AdvertiserResource;
use App\Http\Resources\AdvertPackageResource as AdvertPackageResource;
use App\Models\Advertisingpackage;
use App\Models\Advertisment;
use App\Models\Owner;
use Illuminate\Http\Request;

class AdvertisersController extends Controller
{

    public function advertList(){

        $advertlist = Advertisingpackage::where('active',true)->get();
        // dd($advertlist);
        return response()->json([ 
            'status'=>'success',
            'advert_list' => AdvertPackageResource::collection($advertlist),
            ],200);

    }

    public function index(){
        $advertisers = Owner::leftJoin('advertisments','owners.id','=','advertisments.id')
        ->select('owners.*')
        ->where('advertisments.active',true)
        ->get();
        //dd($advertisers);
        if($advertisers){
            return response()->json([ 
                'status'=>'success',
                'advertisers' => AdvertiserResource::collection($advertisers),
                ],200);
        }
    }
}
