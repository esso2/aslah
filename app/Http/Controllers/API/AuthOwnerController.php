<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateOwner;
use App\Models\Owner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;


class AuthOwnerController extends Controller
{
    public function register(Request $request)
    {

        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'phone' => 'required|string|unique:users',
            'password' => 'required|string|min:3',
            'confirm_password' => 'required|string|min:3',
            'google_token' => 'required',
            'commerical' => 'required',
            'address' => 'required',
            'min_salary' => 'required',
            'bank_name' => 'required',
            'bank_account_owner' => 'required',
            'account_number' => 'required',

        ]);
        $owner = Owner::create([
            'name'              => $request->name,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'password'          => bcrypt($request->password),
            'code'              => mt_rand(1000, 9999),
            'google_token'      => $request->google_token,
            'service_id'        => $request->service_id,
            'city_id'           => $request->city_id,
            'commerical'        => $request->commerical,
            'address'           => $request->address,
            'min_salary'        => $request->min_salary,
            'from'              => $request->from,
            'to'                => $request->to,
            'bank_name'         => $request->bank_name,
            'bank_account_owner' => $request->bank_account_owner,
            'account_number'    => $request->account_number,
        ]);
        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/owners/' . $request->image->hashName()));

            $owner->image = $request->image->hashName();
        }
        $tokenResult = $owner->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        if ($owner == true) {

            return response()->json([
                'status' => 'success',
                'message' => 'Must verfiy your account!',
                'owner_id' =>  $owner->id,
                'type' => 'owner',
                'phone' => $data['phone'],
                'google_token' => $data['google_token'],
                'code' => $owner->code,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ], 200);
        } else {

            return response()->json([
                'status' => 'Error',
                'message' => 'You are Not Register'
            ]);
        }
    }



    public function verify(Request $request)
    {

        $data = $request->validate([
            'code' => ['required', 'numeric'],
            'owner_id' => ['required', 'string'],
        ]);

        $owner = tap(Owner::where('id', $data['owner_id'])->where('code', $data['code']))->update(['isVerified' => true]);

        if ($owner == true) {
            /* Authenticate user */

            Auth::login($owner->first());

            return response()->json([
                'status' => 'success',
                'message' => 'Phone number verified!'
            ], 200);
        } else {

            return response()->json([
                'status' => 'error',
                'message' => 'Invalid verification code entered'
            ]);
        }
    }

    // resnt code verify

    public function resetCode(Request $request)
    {
        $owner = Owner::where('phone', $request->phone)->first();
        //dd($owner);
        if ($owner) {
            return response()->json([
                'status' => 'success',
                'owner_id' =>  $owner->id,
                'phone' => $owner->phone,
                'code' => $owner->code
            ], 200);
        }
    }

    // update password

    public function UpdatePassword(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|string|min:3',
            'password' => 'required|string|min:3',
            'confirm_password' => 'required|string|min:3',
        ]);

        $owner = Owner::where('phone', $data['phone'])->first();

        $owner->update([
            'password' => Hash::make($data['password']),
        ]);

        $owner->save();

        if ($owner) {
            return response()->json([
                'status' => 'success',
                'message' => 'Password Updated'
            ], 200);
        }
    }

    // Owner Login functions

    public function login(Request $request)
    {

        $request->validate([
            'phone' => 'required', 'string',
            'password' => 'required|string',
            // 'remember_me' => 'boolean'
        ]);

        $user = Owner::where('phone', request()->phone)->first();

        if (!Hash::check(request()->password, $user->password)) {

            return response()->json(['error' => 'Unauthorized'], 401);
        } elseif ($user->isVerified == false) {

            return response()->json([
                'message' => 'not verfiy'
            ], 404);
        } elseif ($user->isVerified == true) {

            // $owner = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            return response()->json([
                'status' => 'success',
                'message' => 'you are loged in',
                'owner_id' => $user->id,
                'type' => 'owner',
                'access_token' => $tokenResult->accessToken,
                'google_token' => $user->google_token,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ], 200);
        }
    }


    // Logout Function

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out'
        ], 200);
    }

    // Profile owner

    public function ownerDetails()
    {
        $owner = auth()->guard('owner')->user();
        // return response()->json(auth()->guard('owner')->user());
        // dd($owner);
        return response()->json([
            'status' => 'success',
            'company_name'=>$owner->name,
            'company_phone'=>$owner->phone,
            'company_email'=>$owner->email,
            'company_min_salary'=>$owner->min_salary,
            'company_commerical'=>$owner->commerical,
            'company_image'=>$owner->image,
            'from'=>$owner->from,
            'to'=>$owner->to,
            'bank_name'=>$owner->bank_name,
            'bank_account_owner'=>$owner->bank_account_owner,
            'account_number'=>$owner->account_number,
            'service_name_ar' => $owner->services->translate('ar')->name,
            'service_name_en' => $owner->services->translate('en')->name,
        ], 200);
    }

    public function updateOwner(UpdateOwner $request, $id)
    {

        // dd($request->all());
        $owner = Owner::findOrFail($id);
        // dd($owner);
        $owner->name                       = $request->name;
        $owner->phone                      = $request->phone;
        $owner->email                      = $request->email;
        $owner->min_salary                 = $request->min_salary;
        $owner->commerical                 = $request->commerical;
        $owner->from                       = $request->from;
        $owner->to                         = $request->to;
        $owner->bank_name                  = $request->bank_name;
        $owner->bank_account_owner         = $request->bank_account_owner;
        $owner->account_number             = $request->account_number;
        $owner->password                   = Hash::make($request->password);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/owners/' . $request->image->hashName()));
            $owner->image = 'uploads/owners/' .  $request->image->hashName();
        }
        if ($request->image_owner) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/image_owner/' . $request->image->hashName()));
            $owner->image = 'uploads/image_owner/' . $request->image->hashName();
        }
        $owner->save();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully Updated'
        ], 200);
    }
}
