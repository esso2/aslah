<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Owner;
// use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){

        $query = $request->get('name');


        $filterResult = Owner::where('name', 'LIKE', '%'. $query. '%')->get();
        
        return response()->json($filterResult);

    }
}
