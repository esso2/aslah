<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Resources\ServicesResource as ServicesResourse;
use App\Http\Resources\SubServiceResource as SubServiceResource;
use App\Http\Resources\EndServiceResource as EndServiceResource;
use App\Http\Resources\OwnerServiceDetailsResource as OwnerServiceDetailsResource;
use App\Models\Owner;
use App\Models\OwnerService;
use App\Models\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index()
    {

        $service = Service::where(['active' => 1, 'parent_id' => null])->get();
        if ($service) {
            return response()->json([
                'message' => 'success',
                'services' => ServicesResourse::collection($service),
            ], 200);
        }

    }

    public function mainserviceslist(){

        $service = Service::where(['active' => 1, 'parent_id' => null])->get();
        if ($service) {
            return response()->json([
                'status' => 'success',
                'services' => ServicesResourse::collection($service),
            ], 200);
        }

    }

    public function subServicesList(Request $request){

        $data = $request->id;
        $service = Service::where(['active' => 1, 'parent_id' => $data])->get();
        if($service){
            return response()->json([
                'status' => 'success',
                'subservices'=> SubServiceResource::collection($service),
            ], 200);
        }

    }

    public function endServicesList(Request $request){

        $data = $request->id;

        $service = Service::where(['active' => 1, 'parent_id' => $data])->get();
        if($service){
            return response()->json([
                'status' => 'success',
                'endservices'=> EndServiceResource::collection($service),
            ], 200);
        }

    }

    public function addEndServicesList(Request $request){

        $data = $request->validate([
            'owner_id' => ['required', 'string'],
            'sub_service_id'=>['required'],
            'end_service_id'=>['required'],
            'price'=>'nullable',
            'description'=>'nullable',
        ]);
        $sub_service = OwnerService::create([
            'owner_id' => $request->owner_id,
            'service_id' => $request->sub_service_id,
        ]);
        $sub_service->save();

        $end_service = OwnerService::create([
            'owner_id' => $request->owner_id,
            'service_id' => $request->end_service_id,
            'price' => $request->price,
            'description' => $request->description,
        ]);
        $end_service->save();

        if($end_service){
            return response()->json([
                'status' => 'success',
                'message' => 'success saved !!',
            ], 200);
        } 
    }

    public function deleteEndServicesList($id){

        $endservice = OwnerService::findOrFail($id);
        $endservice->delete();

        if($endservice){
            return response()->json([
                'status' => 'success',
                'message' => 'success deleted !!',
            ], 200);
        }

    }

    public function ownerServices(){

        $owner = auth()->guard('owner')->user();
        $owners = Owner::where('id',$owner->id)->first();
        return response()->json([
             'services' => OwnerServiceDetailsResource::collection($owners->ownerservices)
        ]);

    }

    public function subList(Request $request){
        $owner_id = $request->owner_id;
        $owners = Service::whereHas('servicesowners', function ($query) use ($owner_id) {
            $query->where('owner_id',$owner_id);
            $query->where('price',null);
        })->get();
        return response()->json([
             'services' => SubServiceResource::collection($owners)
        ]);
    }

    public function endList(Request $request){

        $owner_id = $request->owner_id;
        $service_id = $request->service_id;
        
        $owners = Service::leftJoin('owner_services','services.id','=','owner_services.service_id')
        ->select('services.*','owner_services.price as servprice', 'owner_services.description as servdesc')
        ->where('services.active',true)
        ->where('owner_services.owner_id',$owner_id)
        ->where('services.parent_id',$service_id)
        ->get();
        //dd($owners);
        return response()->json([
              'services' => ServicesResourse::collection($owners)
            //'services' =>  $owners->servicesowners
        ]);

    }

    // whereHas('servicesowners', function ($query) use ($owner_id,$service_id) {
    //     $query->where('owner_id',$owner_id);
    //     $query->where('services.parent_id',$service_id);
    // })->with(array('servicesowners' => function($query) {
    //     $query->select('owner_services.price','description');
    // }))->get();

}
