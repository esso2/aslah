<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AboutusResource as AboutusResource ;
use App\Models\Aboutus;
use Illuminate\Http\Request;

class AboutusController extends Controller
{
    public function aboutus(){

        $aboutus = Aboutus::findorFail(1);
        if($aboutus){
            return response()->json([ 
                'status'=>'success',
                'id' => $aboutus->id,
                'aboutuses_ar' => $aboutus->translate('ar')->aboutuses,
                'aboutuses_en' => $aboutus->translate('en')->aboutuses,
                ],200);
        }

    }
}
