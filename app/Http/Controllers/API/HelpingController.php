<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Help;
use App\Http\Resources\HelpingResource as HelpingResource;
use Illuminate\Http\Request;

class HelpingController extends Controller
{

    public function index()
    {
        $helping = Help::findorFail(1);
        if($helping){
            return response()->json([
                'status'=>'success', 
                'id' => $helping->id,
                'support_number' => $helping->support_number,
                'support_number' => $helping->support_number,
                'vedio_link' => $helping->vedio_link,
                'question_ar' => $helping->translate('ar')->question,
                'question_en' => $helping->translate('en')->question,
                ],200);
        }
    }

}
