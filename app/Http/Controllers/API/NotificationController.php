<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function newindex()
    {
        $user        = Auth::guard('api')->user();
        $profile = Notification::where('user_id',$user->id)->get();
        return response()->json([
                'data' => $profile,
                'message' => 'success',
                ],200);
    }
    
    public function delnotification(Request $request)
    {
            $noty = Notification::where('id',$request->notification_id)->first();
            $noty->delete();
            if($noty){
                return response()->json([
                    'status' => 'success',
                    'message' => 'تم الحذف بنجاح',
                    ],200);
            }else{
                return response()->json([
                    'status' => 'success',
                    'message' => 'لم يتم الحذف',
                    ],200);
            }
    }
}
