<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\OwnerResource as OwnerResource;
use App\Models\Owner;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OwnerController extends Controller
{

    public function index()
    {
        $owners = Owner::where('active',true)->with('services')->get();
        
        //dd($owners);
        if($owners){
            return response()->json([ 
                'message'=>'success',
                'owners' => OwnerResource::collection($owners),
                ],200);
        }
    }


    public function ownerDetails(Request $request){

        $owner_id = $request->id;
        $owner = Owner::where(['id'=>$owner_id,'active'=>true])->first();
        // $avgRate = Rate::avg('rate');
        $rate = DB::table('rates')
        ->where('owner_id', $owner_id)
        ->avg('rate');
        $rateaverage = (int)$rate;
            return response()->json([ 
                'status'=>'success',
                'owner_id' => $owner->id,
                'owner_image' => $owner->image,
                'owner_name' => $owner->name,
                'owner_phone' => $owner->phone,
                'owner_service' =>  [
                    'id' => $owner->services->id,
                    'name_ar' => $owner->services->translate('ar')->name,
                    'name_en' => $owner->services->translate('en')->name,
                ],
                'owner_city' =>  [
                    'id' => $owner->cities->id,
                    'name_ar' => $owner->cities->translate('ar')->name,
                    'name_en' => $owner->cities->translate('en')->name,
                ],
                'owner_name' => $owner->name,
                'avilable_from' => $owner->from,
                'avilable_to' => $owner->to,
                'min_salary' => $owner->min_salary,
                'rate' =>$rateaverage
            ],200);
    }
}
