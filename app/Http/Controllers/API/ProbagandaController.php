<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Propaganda;
use App\Http\Resources\ProbagandaResource as ProbagandaResource;
use Illuminate\Http\Request;

class ProbagandaController extends Controller
{

    public function index()
    {
        $propagandas = Propaganda::findorFail(1);
        if($propagandas){
            return response()->json([ 
                'status'=>'success',
                'id' => $propagandas->id,
                'propagandas_ar' => $propagandas->translate('ar')->propagandas,
                'propagandas_en' => $propagandas->translate('en')->propagandas,
                ],200);
        }
    }

}
