<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Storecomplain;
use App\Models\Complaints;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class ComplaintController extends Controller
{

    public function storecomp(Storecomplain $request)
    {

       // dd($request->all());

        $status = Complaints::create([
            'name'   => $request->input('name'),
            'phone'   => $request->input('phone'),
            'message'  => $request->input('message'),
            'order_id'  => $request->input('order_id'),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/complain/' . $request->image->hashName()));

            $status->image ='uploads/complain/' .  $request->image->hashName();
        }

        $status->save();

        if($status){
            return response()->json([
                'status'=>'success',
                'message'=>'complaint send successfully',
            ]);
        }
    }

}
