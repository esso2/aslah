<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class UserOrdersController extends Controller
{
    public function currentOrder(){
        $user = auth()->user();
        $waiting_order = Order::where('user_id',$user->id)
        ->where(function($query){
            $query->orWhere('orders.action', 'accept');
            $query->orWhere('orders.action', 'wait');
        })->get();

        if($waiting_order){
            return response()->json([
                'status'=>'success',
                'order'=>$waiting_order,
            ],200);
        }
    }

    public function finishedOrder(){
        $user = auth()->user();
        $finished_order = Order::where('user_id',$user->id)
        ->where(function($query){
            $query->orWhere('orders.action', 'finished');
        })->get();
            if($finished_order){
                return response()->json([
                    'status'=>'success',
                    'order'=>$finished_order,
                ],200);
            }
    }

    public function orderDetails(Request $request){
        $order_id = $request->id;
        $order = Order::where(['id'=>$order_id])->first();
        // dd($order);
        return response()->json([
            'status'=>'success',
            'owner_name'=>$order->owners->name,
            'services_name'=>$order->services->name,
            'min_salary'=>$order->owners->min_salary,
            // 'rate'=>$order->owners->rates->rate,
            'order_number'=>$order->id,
            'total_price'=>$order->total_price,
            'address'=>$order->address,
            'res_time'=>$order->res_time,
            'res_date'=>$order->res_date,
            'image'=>$order->image,
            'notes'=>$order->note,
        ],200);
    }

}
