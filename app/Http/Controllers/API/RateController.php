<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRate;
use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    public function rateOwner(StoreRate $request){

        $status = Rate::create([
            'rate'   => $request->input('rate'),
            'user_id'  => $request->input('user_id'),
            'owner_id'  => $request->input('owner_id'),
        ]);
        $status->save();

        if($status){
            return response()->json([
                'status'=>'success',
                'message'=>'Rate Created Successfully',
            ]);
        }
    }
}
