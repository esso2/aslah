<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\BannersResource as BannersResource;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannersController extends Controller
{

    public function index()
    {
        $banners = Banner::where('active',true)->get();
        if($banners){
            return response()->json([ 
                'message'=>'success',
                'banners' => BannersResource::collection($banners),
                ],200);
        }
    }

}
