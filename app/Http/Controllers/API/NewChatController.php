<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\Conversation;
use App\Models\Notification;
use App\Models\Owner;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewChatController extends Controller
{
  
    public function addConversation(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'massage' => 'required',
          'sender_id' => 'required|exists:owners,id',
          'receiver_id' => 'required|exists:users,id',
        ]);

        if($validator->fails())
        {
            return response()->json(['msg' => 'error' , 'data' => $validator->errors()]);     
        }

        $conversation = Conversation::
        where('sender_id',$request['sender_id'])
        ->where('receiver_id',$request['receiver_id'])->first();
        
        if(empty($conversation)){
        $conversation = Conversation::
        where('receiver_id',$request['sender_id'])
        ->where('sender_id',$request['receiver_id'])->first();
        }

        if(empty($conversation)){
        $conversation = new Conversation; 
        $conversation->sender_id = $request['sender_id'];
        $conversation->receiver_id = $request['receiver_id'];
        $conversation->save();
        }

        $chat = new chat; 
        $chat->massage = $request['massage'];
        $chat->conversation_id = $conversation->id;
        $chat->sender_id = $request['sender_id'];
        $chat->receiver_id = $request['receiver_id'];
        $chat->save();

        $chat = Chat::where('conversation_id',$conversation->id)
        ->orderBy('created_at','asc')->with('sender')
        ->with('receiver')
        ->get();

            if($chat){
            $not = new Notification();
            $not->title = "لديك اشعار جديد"; 
            $not->body = "لقد تلقيت رساله جديده";
            $not->title_en= "new notification"; 
            $not->body_en = "you have new order";
            $not->user_id = $request['sender_id'];
            $not->save();
             
            $to = Owner::where('id',$request['sender_id'])->value('google_token');
     
            // $message = [
            //     "user_id"=>$request->user_id,
            //     "sender_name"=>$request->owner_id
            //     ];
    
     
            $fields = array(

                'to'   => $to,
                "notification"=>[
                    "title"=>"لديك رساله جديد ",
                    "body"=>"رساله جديده"
                    ],
            
            );
     
            // Set POST variables
            $url = 'https://fcm.googleapis.com/fcm/send';
            $ch = curl_init();

            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; ',
                        'Authorization: key = '));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec( $ch );
            if ( $result === false ) {
            die( 'Curl failed: ' . curl_error( $ch ) );
            }
            // Close connection
            curl_close( $ch );

            return response()->json([
                    'msg' => 'success',
                    'data' =>$chat
            ]);

            }else{
            return Response()->json([
                    'data'   => null,
                    'errors'       => [' '],
                    'message'       => 'order not found', 
            ]);

            }
    }



    public function addMassage(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'type' => 'required',
          'sender_id' => 'required|exists:owners,id',
          'receiver_id' => 'required|exists:users,id',
          'conversation_id' => 'required|exists:conversations,id',
        ]);

        if($validator->fails())
        {
            return response()->json(['msg' => 'error' , 'data' => $validator->errors()]);        
        }

        if($request['type']==0){
        	if(empty($request['massage'])){      
        return response()->json(['msg' => 'error' , 'data' => 'The massage field is required.']); 
        	}else{
        		$chat = new chat; 
                $chat->massage = $request['massage'];
                $chat->conversation_id = $request['conversation_id'];
                $chat->sender_id = $request['sender_id'];
                $chat->receiver_id = $request['receiver_id'];
                $chat->save();
        	}

        }elseif($request['type']==1){
        	if(empty($request['file'])){
        	return response()->json(['msg' => 'error' , 'data' => 'The file field is required.']);  
        	}else{ 
        	$images = $request['file'];  
            $chat = new chat; 
        	    if($images)
                {
                    $img_name = 'uploads/chats/'.rand(0, 999) . '.' . $images->getClientOriginalExtension();
                     $images->move(base_path('public/uploads/chats'), $img_name);
                	$chat->file = $img_name;
            	}
                $chat->type = $request['type'];
                $chat->conversation_id = $request['conversation_id'];
                $chat->sender_id = $request['sender_id'];
                $chat->receiver_id = $request['receiver_id'];
                $chat->save();
        	}
        }
        $chats = Chat::where('conversation_id',$request['conversation_id'])
        ->orderBy('created_at','asc')->with('sender')->with('receiver')->get();
        
        if($chats){    
            $not = new Notification;
            $not->title = "لديك اشعار جديد"; 
            $not->body = "لقد تلقيت رساله جديده";
            $not->title_en= "new notification"; 
            $not->body_en = "you have new order";
            $not->owner_id = $request['sender_id'];
            $not->save();
             
            $to = User::where('id',$request['receiver_id'])->value('google_token');
     
            // $message = [
            //     "user_id"=>$request->user_id,
            //     "sender_name"=>$request->owner_id
            //     ];

            $fields = array(
            'to'   => $to,
            "notification"=>[
                    "title"=>"لديك رساله جديد ",
                    "body"=>"رساله جديده"
                    ],
            );

      $url = 'https://fcm.googleapis.com/fcm/send';
      $ch = curl_init();

      curl_setopt( $ch, CURLOPT_URL, $url );
      curl_setopt( $ch, CURLOPT_POST, true );
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; ',
                      'Authorization: key = '));
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
      $result = curl_exec( $ch );
      if ( $result === false ) {
         die( 'Curl failed: ' . curl_error( $ch ) );
      }
      curl_close( $ch );
        return Response()->json([
            'data'  => [
                'chat'  => $chats,
            ],
            'message'   => 'success',
        ]);
        
        }else{
        return Response()->json([
            'data'   => null,
            'errors'       => [' '],
            'message'       => 'order not found',
            
        ]);          
        }
            //  return response()->json([
            //         'msg' => 'success',
            //         'data' =>$chats]);
    }

    public function getConversationById(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'conversation_id' => 'required|exists:conversations,id',
        ]);
        if($validator->fails())
        {
         return response()->json(['msg' => 'error' , 'data' => $validator->errors()]);       
        }
    $chats = Chat::where('conversation_id',$request['conversation_id'])->orderBy('created_at','asc')  ->with('sender')->with('receiver')->get();
        return response()->json([
            'msg' => 'success',
            'data' =>$chats
        ]);
    }

    public function getAllConversation(Request $request)
    {
      $conversation=Conversation::orderBy('created_at','asc')->get();
      return $this->sendResponse($conversation,'success');
    }

    public function getConversationByUser_id(Request $request)
    {
    	$validator = Validator::make($request->all(), [
          'user_id' => 'required|exists:users,id',
        ]);
        if($validator->fails())
        {
                return response()->json(['msg' => 'error' , 'data' => $validator->errors()]);        
        }
      $conversation=Conversation::where('sender_id',$request['user_id'])->orwhere('receiver_id',$request['user_id'])->orderBy('created_at','asc')->with('sender')->with('receiver')->with('lastchat')->get();
      return response()->json([
        'msg' => 'success',
        'data' =>$conversation
     ]);
    }

    public function getListChatByUser_id(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'user_id' => 'required|exists:users,id',
        ]);
        if($validator->fails())
        {
            return response()->json(['msg' => 'error' , 'data' => $validator->errors()]);      
        }
           $conversation = Conversation::where('sender_id',$request['user_id'])->first();
            if(empty($conversation)){
            $conversation = Conversation::where('receiver_id',$request['user_id'])->first();
            }
      $chat=chat::where('conversation_id',$conversation->id)->orderBy('created_at','asc')->with('sender')->with('receiver')->get();
      return response()->json([
        'msg' => 'success',
        'data' =>$chat]);
    }
}
