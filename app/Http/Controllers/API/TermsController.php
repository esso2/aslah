<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\TermsResource as TermsResource;
use App\Models\Term;
use Illuminate\Http\Request;

class TermsController extends Controller
{

    public function index()
    {
        $propagandas = Term::findorFail(1);
        if($propagandas){
            return response()->json([ 
                'status'=>'success',
                'id' => $propagandas->id,
                'terms_ar' => $propagandas->translate('ar')->terms,
                'terms_en' => $propagandas->translate('en')->terms,
                ],200);
        }
    }

}
