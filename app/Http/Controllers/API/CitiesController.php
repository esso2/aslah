<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function index(){
        $cities = City::where('active',true)->get();
        if($cities){
            return response()->json([ 
                'status'=>'success',
                'cities' => CityResource::collection($cities),
                ],200);
        }
    }
}
