<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;

class OwnerOrdersController extends Controller
{
    public function currentOrder(){
        $owner = auth()->guard('owner')->user();
        // auth()->guard('owner')->user()
        $waiting_order = Order::where('owner_id',$owner->id)
        ->where(function($query){
            $query->orWhere('orders.action', 'accept');
            $query->orWhere('orders.action', 'wait');
        })->get();
            if($waiting_order){
                return response()->json([
                    'status'=>'success',
                    'order'=>$waiting_order,
                ],200);
            }
    }

    public function finishedOrder(){
        $owner = auth()->guard('owner')->user();
        $finished_order = Order::where('owner_id',$owner->id)
        ->where(function($query){
            $query->orWhere('orders.action', 'finished');
        })->get();
            if($finished_order){
                return response()->json([
                    'status'=>'success',
                    'order'=>$finished_order,
                ],200);
            }
    }

    public function ownerOrderDetails(Request $request){

        $order_id = $request->id;
        $order = Order::where(['id'=>$order_id])->first();
        return response()->json([
            'status'=>'success',
            'user_name'=>$order->users->name,
            'services_name'=>$order->services->name,
            'min_salary'=>$order->owners->min_salary,
            'order_number'=>$order->id,
            'total_price'=>$order->total_price,
            'address'=>$order->address,
            'res_time'=>$order->res_time,
            'res_date'=>$order->res_date,
            'image'=>$order->image,
            'notes'=>$order->note,
        ],200);

    }

    public function acceptOrder(Request $request){
        $order_id = $request->id;
        $order = Order::where('id',$order_id)->first();
        $order->update([
            'action'      => $request->action,
        ]);
            $not = new Notification();
            $not->title = "تم الموافقه على طلبك"; 
            $not->body = "تم الموافقه على طلبك";
            $not->title_en= "Your Order Accept"; 
            $not->body_en = "Your Order Accept";
            $not->user_id = $order->user_id;
            $not->save();

            $to = User::where([['id',$order->user_id]])->value('google_token');

            $message = [
                "owner_id"=>$order->owner_id,
                "sender_name"=>$order->user_id
                ];

            $fields = array(
            'to'   => $to,
            "notification"=>[
                "title"=>"تم الموافقه على طلبك",
                "body"=>"تم الموافقه على طلبك"
                ],
            'data'=>$message
            );

            // Set POST variables
            $url = 'https://fcm.googleapis.com/fcm/send';
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; ',
            'Authorization: key = AAAAZpIzcCw:APA91bFeipLTa-TO-n3SrHZoEps8V0inpbHUXzp0U2Xqcw7ic8zc5gLDUiUgESWUZWYj3QsaqSMefjMSnpZVWz6lY7DZgn2SNeYZ_JbpfZ5fYUsA_4D2xNUcrUdUgG2U4ViGHuNAKU5q'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            // Disabling SSL Certificate support temporarly
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec( $ch );
            if ( $result === false ) {
            die( 'Curl failed: ' . curl_error( $ch ) );
            }
            curl_close( $ch );


        return response()->json([
            'status'=>'success',
            'message'=>'order accepted successfully',
        ],200);
    }

    public function refuseOrder(Request $request){
        $order_id = $request->id;
        $order = Order::where('id',$order_id)->first();
        $order->update([
            'action'      => $request->action,
        ]);
        return response()->json([
            'status'=>'success',
            'message'=>'order has been finished',
        ],200);
    }

}
