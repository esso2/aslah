<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateTerm;
use App\Models\Term;
use Illuminate\Http\Request;

class TermsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:term-read')->only(['index']);
        $this->middleware('permission:term-update')->only(['update']);
    }
    public function index()
    {
        $terming = Term::first();
        return view('admin.terms.edit', compact('terming'));
    }

    public function update(UpdateTerm $request)
    {
        $terming = Term::findOrFail(1);
        $terming->update([
            'ar' => [
                'terms'       => $request->input('ar.terms'),
            ],
            'en' => [
                'terms'       => $request->input('en.terms'),
            ],
        ]);

        $terming->save();
        return response()->json(['status'=>'success','data'=>$terming]);
        
    }
}
