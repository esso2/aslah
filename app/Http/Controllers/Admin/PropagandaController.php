<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePropaganda;
use App\Models\Propaganda;
use Illuminate\Http\Request;

class PropagandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:propaganda-read')->only(['index']);
        $this->middleware('permission:propaganda-update')->only(['update']);
    }
    public function index()
    {
        $propaganding = Propaganda::first();
        return view('admin.propagandas.edit', compact('propaganding'));
    }

    public function update(UpdatePropaganda $request)
    {
        $propaganding = Propaganda::findOrFail(1);
        $propaganding->update([
            'ar' => [
                'propagandas'       => $request->input('ar.propagandas'),
            ],
            'en' => [
                'propagandas'       => $request->input('en.propagandas'),
            ],
        ]);

        $propaganding->save();
        return response()->json(['status'=>'success','data'=>$propaganding]);
        
    }
}
