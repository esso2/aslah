<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateHelp;
use App\Models\Help;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:help-read')->only(['index']);
        $this->middleware('permission:help-update')->only(['update']);
    }
    public function index()
    {
        $helping = Help::first();
        return view('admin.helps.edit', compact('helping'));
    }

    public function update(UpdateHelp $request)
    {
        $helping = Help::findOrFail(1);
        $helping->update([
            'ar' => [
                'question'       => $request->input('ar.question'),
            ],
            'en' => [
                'question'       => $request->input('en.question'),
            ],
            'support_number' => $request->support_number,
            'vedio_link' => $request->vedio_link,
        ]);

        $helping->save();
        return response()->json(['status'=>'success','data'=>$helping]);
        
    }

}
