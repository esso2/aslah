<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Models\Address;
use Illuminate\Support\Facades\Storage;
use App\Models\City;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
class UsersController extends Controller
{

    
    public function __construct()
    {
        $this->middleware('permission:users-read')->only(['index']);
        $this->middleware('permission:users-create')->only(['create', 'store']);
        $this->middleware('permission:users-update')->only(['edit', 'update']);
        $this->middleware('permission:users-delete')->only(['destroy']);
    }

  
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($query) {
                return '<img src=' . $query->image_path . ' border="0" style=" width: 80px; height: 80px;" class="image-show img-circle" />';
            })
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
                    <div align="center">
                    <label class="switch">
                    <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                        <div class="slider round">
                            <span class="on">ON</span>
                            <span class="off">OFF</span>
                        </div>
                    </label>
                </div>';
                } else {
                    $btn = '
                        <div align="center">
                        <label class="switch">
                        <input data-id="' . $query->id . '" type="checkbox" id="check">
                            <div class="slider round">
                                <span class="on">ON</span>
                                <span class="off">OFF</span>
                            </div>
                        </label>
                    </div>';
                }

                return $btn;
            })
            ->addColumn('action', function($row){
                
            if (Auth::guard('admin')->user()->hasPermission('users-update')){
             $btn ='<a href="' .route("users.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></a> &nbsp;';
            }else{
             $btn = '<a href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
            }

            if (Auth::guard('admin')->user()->hasPermission('users-read')){
             $btn =  $btn .'<a type="button" class="btn btn-default btn-icon waves-effect details" data-id="' . $row->id . '" data-toggle="modal" href="#Modal-tab"><i class="icofont icofont-eye-alt"></i> </a> &nbsp;';
            }else{
             $btn =   $btn .'<a href="" class="btn btn-primary btn-xs disabled">Show</i></a>';
            }
    
            if (Auth::guard('admin')->user()->hasPermission('users-delete')){
                $btn = $btn. 
                '<form class="delete"  action="' . route("users.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
    
                        return $btn;
                    })
    
                    ->rawColumns(['action','active','image'])
                    ->make(true);
    
        }

        
    
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ctities = City::all();
        return view('admin.users.create', compact('ctities'));
    }

  


    public function store(StoreUser $request)
    {
            $user = User::create([
                'name'      => $request->input('name'),
                'phone'     => $request->input('phone'),
                'email'     => $request->input('email'),
                'password'  => Hash::make($request->password),
                'isVerified' => 1
            ]);

            if ($request->image) {
                Image::make($request->image)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('uploads/users/' . $request->image->hashName()));
    
                $user->image ='uploads/users/' . $request->image->hashName();
            }

           $user->save();

        return response()->json(['status'=>'success','data'=>$user]);
    }




    public function show(Request $request)
    { 

    }




    public function UserStatus(Request $request)
    {
        $user = User::find($request->user_id);
        $user->active = $request->active;
        $user->save();
        return response()->json(['status' => 'success', 'data' => $user]);
    }
    


    public function UserAddress(Request $request)
    {
       $useraddress = Address::where('user_id',$request->user_id)->get();

       return json_decode($useraddress);
    }


    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.edit',compact('user'));
    }

  

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'name'      => $request->input('name'),
            'phone'     => $request->input('phone'),
            'email'     => $request->input('email'),
            'password'  => Hash::make($request->password),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/' . $request->image->hashName()));

            $user->image ='uploads/users/' . $request->image->hashName();
        }

        $user->save();
        return response()->json(['status'=>'success','data'=>$user]);
    }

  

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->image != 'default.png') {
            Storage::disk('public_uploads')->delete('/users/' . $user->image);
        }
            
        $user->delete();
        return response()->json(['status'=>'success']);
    }
}
