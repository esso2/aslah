<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

class BannersController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:banners-read')->only(['index']);
        $this->middleware('permission:banners-create')->only(['create', 'store']);
        $this->middleware('permission:banners-update')->only(['edit', 'update']);
        $this->middleware('permission:banners-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Banner::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($query) {
                return '
                <div align="center">
                <img src=' . $query->image_path . ' border="0" style=" width: 290px; height: 180px;" class="image-show" />
                </div>
                ';
            })
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
                 </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
              </div>';
                }
    
                return $btn;
            })
            ->addColumn('action', function($row){
                
                if (Auth::guard('admin')->user()->hasPermission('banners-update')){
                $btn ='<a href="' .route("banners.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></i></a> &nbsp;';
                }else{
                $btn = '<a href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
                }
                if (Auth::guard('admin')->user()->hasPermission('banners-delete')){
                    $btn = $btn.
                    '<form class="delete"  action="' . route("banners.destroy", $row->id) . '"  method="POST" id="delform"
                    style="display: inline-block; right: 50px;" >
                    <input name="_method" type="hidden" value="DELETE">
                    <input type="hidden" name="_token" value="' . csrf_token() . '">
                    <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                        </form>';
                }else{
                    $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
                }
        
                        return $btn;
                    })
    
                    ->rawColumns(['action','active','image'])
                    ->make(true);
    
        }
    
        return view('admin.banners.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = Banner::create([
            'banner_url'      => $request->input('banner_url'),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/banners/' . $request->image->hashName()));

            $banner->image ='uploads/banners/' . $request->image->hashName();
        }

       $banner->save();

    return response()->json(['status'=>'success','data'=>$banner]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    public function BannerStatus(Request $request)
    {
        $banner = Banner::find($request->banner_id);
        $banner->active = $request->active;
        $banner->save();
        
        return response()->json(['status' => 'success', 'data' => $banner]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('admin.banners.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::findOrFail($id);
        $banner->update([
            'banner_url'      => $request->input('banner_url'),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/banners/' . $request->image->hashName()));

            $banner->image = 'uploads/banners/' . $request->image->hashName();
        }

        $banner->save();
        return response()->json(['status'=>'success','data'=>$banner]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);

        if ($banner->image != 'default.png') {
            Storage::disk('public_uploads')->delete('/banners/' . $banner->image);
        }
            
        $banner->delete();
        return response()->json(['status'=>'success']);
    }
}
