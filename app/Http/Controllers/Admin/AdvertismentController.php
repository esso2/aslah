<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisingpackage;
use App\Models\Advertisment;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\Owner;
use Illuminate\Http\Request;

class AdvertismentController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:advert-read')->only(['index']);
        $this->middleware('permission:advert-create')->only(['create','store']);
        $this->middleware('permission:advert-update')->only(['edit','update']);
        $this->middleware('permission:advert-delete')->only(['destroy']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Advertisment::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('owner_id', function ($query) {
                $owner_name = Owner::where('id',$query->owner_id)->first();
                return $owner_name->name;
            })
            ->editColumn('package_id', function ($query) {
                $package = Advertisingpackage::where('id',$query->package_id)->first();
                return $package->period;
            })
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
            <div align="center">
            <label class="switch">
            <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                <div class="slider round">
                    <span class="on">ON</span>
                    <span class="off">OFF</span>
                </div>
            </label>
          </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
              </div>';
                }

                return $btn;
            })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('advert-update')){
            $btn ='<a href="' .route("advertisments.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></i></a> &nbsp;';
            }else{
            $btn = '<a  href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
            }
    
            if (Auth::guard('admin')->user()->hasPermission('advert-delete')){
                $btn = $btn.
                '<form class="delete"  action="' . route("advertisments.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }

                        return $btn;
                    })

                    ->rawColumns(['action','active'])
                    ->make(true);

            }
        return view('admin.advertisments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $owners = Owner::get();
        $packages = Advertisingpackage::get();
        return view('admin.advertisments.create', compact('owners','packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $owner = Advertisment::create([
            'owner_id'       => $request->input('owner_id'),
            'package_id'      => $request->input('package_id'),
        ]);

        $owner->save();

        return response()->json(['status'=>'success','data'=>$owner]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function advertismentStatus(Request $request)
    {
        $advertisment = Advertisment::find($request->ads_id);
        $advertisment->active = $request->active;
        $advertisment->save();
        
        return response()->json(['status' => 'success', 'data' => $advertisment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $owners = Owner::get();
        $packages = Advertisingpackage::get();

        $advertisment = Advertisment::findOrFail($id);
        return view('admin.advertisments.edit', compact('owners','packages','advertisment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advertisment = Advertisment::findOrFail($id);

        $advertisment->update([
            'owner_id'       => $request->input('owner_id'),
            'package_id'      => $request->input('package_id'),
        ]);

        $advertisment->save();
        return response()->json(['status'=>'success','data'=>$advertisment]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisment = Advertisment::findOrFail($id);
        $advertisment->delete();
        return response()->json(['status'=>'success']);
    }
}
