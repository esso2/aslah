<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePackage;
use App\Models\Advertisingpackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class AdvertisingpackagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:package-read')->only(['index']);
        $this->middleware('permission:package-create')->only(['create', 'store']);
        $this->middleware('permission:package-update')->only(['edit', 'update']);
        $this->middleware('permission:package-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Advertisingpackage::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
                 </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
              </div>';
                }
    
                return $btn;
            })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('package-update')){
             $btn ='<a href="' .route("packages.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></i></a> &nbsp;';
            }else{
             $btn = '<a  href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
            }
    
            if (Auth::guard('admin')->user()->hasPermission('package-delete')){
                $btn = $btn.
                '<form class="delete"  action="' . route("packages.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
    
                        return $btn;
                    })
    
                    ->rawColumns(['action','active'])
                    ->make(true);
    
            }
        return view('admin.packages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackage $request)
    {
        $package = Advertisingpackage::create([

                'period'       => $request->input('period'),
                'price'       => $request->input('price'),

        ]);

    $package->save();

    return response()->json(['status'=>'success','data'=>$package]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function packageStatus(Request $request)
    {
        $package = Advertisingpackage::find($request->package_id);
        $package->active = $request->active;
        $package->save();
        
        return response()->json(['status' => 'success', 'data' => $package]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Advertisingpackage::findOrFail($id);
        return view('admin.packages.edit',compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = Advertisingpackage::findOrFail($id);
        $package->update([
            'period'       => $request->input('period'),
            'price'       => $request->input('price'),
        ]);

        $package->save();
        return response()->json(['status'=>'success','data'=>$package]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Advertisingpackage::findOrFail($id);
        $package->delete();
        return response()->json(['status'=>'success']);
    }
}
