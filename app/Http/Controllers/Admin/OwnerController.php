<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOwner;
use Illuminate\Support\Facades\Storage;
use App\Models\City;
use App\Models\Owner;
use App\Models\OwnerService;
use App\Models\Service;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

class OwnerController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:owners-read')->only(['index']);
        $this->middleware('permission:owners-create')->only(['create', 'store']);
        $this->middleware('permission:owners-update')->only(['edit', 'update']);
        $this->middleware('permission:owners-delete')->only(['destroy']);
    }



    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Owner::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($query) {
                return '<img src=' . $query->image_path . ' border="0" style=" width: 80px; height: 80px;" class="image-show img-circle" />';
            })
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
            <div align="center">
            <label class="switch">
            <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                <div class="slider round">
                    <span class="on">ON</span>
                    <span class="off">OFF</span>
                </div>
            </label>
         </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
            </div>';
                }

                return $btn;
            })
            ->addColumn('action', function($row){
                
            if (Auth::guard('admin')->user()->hasPermission('owners-update')){
            $btn ='<a href="' .route("owners.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></i></a> &nbsp;';
            }else{
             $btn = '<a href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
            }

            if (Auth::guard('admin')->user()->hasPermission('owners-read')){
                $btn =  $btn .'<a href="' .route("owners.show", $row->id). '" type="button" class="btn btn-default btn-icon waves-effect" ><i class="icofont icofont-eye-alt"></i></a> &nbsp;';
            }else{
                $btn =   $btn .'<a href="" class="btn btn-primary btn-xs disabled">Show</i></a>';
            }

            if (Auth::guard('admin')->user()->hasPermission('owners-read')){
                $btn =  $btn .'<a href="' .route("owners.balances", $row->id). '" type="button" class="btn btn-default btn-icon waves-effect" ><i class="icofont-coins"></i></a> &nbsp;';
            }else{
                $btn =   $btn .'<a href="" class="btn btn-default btn-icon waves-effect disabled">Show</i></a>';
            }
    
            if (Auth::guard('admin')->user()->hasPermission('owners-delete')){
                $btn = $btn.
                '<form class="delete"  action="' . route("owners.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }

                        return $btn;
                    })

                    ->rawColumns(['action','active','image'])
                    ->make(true);

        }

        return view('admin.owners.index');
    }

    public function create()
    {
        $cities = City::all();
        $services = Service::where('parent_id',null)->get();
        return view('admin.owners.create', compact('cities','services'));
    }

    public function store(StoreOwner $request)
    {
        $owner = Owner::create([
            'name'       => $request->input('name'),
            'email'      => $request->input('email'),
            'phone'      => $request->input('phone'),
            'commerical' => $request->input('commerical'),
            'address'    => $request->input('address'),
            'min_salary' => $request->input('min_salary'),
            'service_id' => $request->input('service_id'),
            'city_id'    => $request->input('city_id'),
            'from'       => $request->input('from'),
            'to'         => $request->input('to'),
            'bank_name'         => $request->input('bank_name'),
            'bank_account_owner'=> $request->input('bank_account_owner'),
            'account_number'    => $request->input('account_number'),
            'password'   => Hash::make($request->password),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/owners/' . $request->image->hashName()));

            $owner->image = 'uploads/owners/' . $request->image->hashName();
        }

        if ($request->image_owner) {
            Image::make($request->image_owner)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/owners/' . $request->image_owner->hashName()));

            $owner->image ='uploads/owners/' . $request->image_owner->hashName();
        }

       $owner->save();

       return response()->json(['status'=>'success','data'=>$owner]);
    }

 

    public function show($id)
    {
        // $owner = Owner::findOrFail($id);
        // $services = OwnerService::get();
        // return view('admin.owners.show',compact('owner','services'));
    }

    public function OwnersBalance($id){
        return view('admin.owners.balance');
    }


    public function OwnerStatus(Request $request)
    {
        $owner = Owner::find($request->owner_id);
        $owner->active = $request->active;
        $owner->save();
        
        return response()->json(['status' => 'success', 'data' => $owner]);
    }



    public function edit($id)
    {
        $services = Service::where('parent_id',null)->get();

        $owner = Owner::findOrFail($id);
        return view('admin.owners.edit',compact('owner','services'));
    }



    public function update(Request $request, $id)
    {
        $owner = Owner::findOrFail($id);

        $owner->update([
            'name'           => $request->input('name'),
            'phone'          => $request->input('phone'),
            'address'        => $request->input('address'),
            'from'           => $request->input('from'),
            'to'             => $request->input('to'),
            'min_salary'     => $request->input('min_salary'),
            'national_number'=> $request->input('national_number'),
            'service_id'     => $request->input('service_id'),
            'city_id'        => $request->input('city_id'),
            'from'           => $request->input('from'),
            'to'             => $request->input('to'),
            'bank_name'         => $request->input('bank_name'),
            'bank_account_owner'=> $request->input('bank_account_owner'),
            'account_number'    => $request->input('account_number'),
            'password'       => Hash::make($request->password),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/owners/' . $request->image->hashName()));

            $owner->image = 'uploads/owners/' . $request->image->hashName();
        }
        if ($request->image_owner) {
            Image::make($request->image_owner)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/owners/' . $request->image_owner->hashName()));

            $owner->image_owner = 'uploads/owners/' . $request->image_owner->hashName();
        }

        $owner->save();
        return response()->json(['status'=>'success','data'=>$owner]);
    }



    public function destroy($id)
    {
        $user = Owner::findOrFail($id);

        if ($user->image != 'default.png') {
            Storage::disk('public_uploads')->delete('/owners/' . $user->image);
        }
            
        $user->delete();
        return response()->json(['status'=>'success']);
    }
}
