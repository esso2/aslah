<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;


class ServicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:serv-read')->only(['index']);
        $this->middleware('permission:serv-create')->only(['create', 'store']);
        $this->middleware('permission:serv-update')->only(['edit', 'update']);
        $this->middleware('permission:serv-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Service::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('parent_id',function ($row){
                if($row->parent_id != null){
                    return '<span class="label label-info">'.$row->service->name.'</span>';
                }else{
                    return '<span class="label label-danger">'.__('admin.maincat').'</span>';
                }
            })

            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
            </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
              </div>';
                }

                return $btn;
            })

            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('serv-update')){
             $btn ='<a href="' .route("services.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></i></a> &nbsp;';
            }else{
             $btn = '<a  href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
            }
    
            if (Auth::guard('admin')->user()->hasPermission('serv-delete')){
                $btn = $btn.
                '<form class="delete"  action="' . route("services.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
    
                    return $btn;
                    })
    
                    ->rawColumns(['action','parent_id','active'])
                    ->make(true);
    
            }
        return view('admin.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        return view('admin.services.create',compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = Service::create([

            'ar' => [
                'name'       => $request->input('ar.name'),
            ],
            'en' => [
                'name'       => $request->input('en.name'),
            ],

            'parent_id'     => $request->input('parent_id'),
        ]);
        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/services/' . $request->image->hashName()));

            $service->image ='uploads/services/' . $request->image->hashName();
        }
        
        $service->save();

        return response()->json(['status'=>'success','data'=>$service]);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function ServiceStatus(Request $request)
    {
        $service = Service::find($request->service_id);
        $service->active = $request->active;
        $service->save();
        
        return response()->json(['status' => 'success', 'data' => $service]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = Service::all();
        $service = Service::findOrFail($id);
        return view('admin.services.edit',compact('service','services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        $service->update([
            'ar' => [
                'name'       => $request->input('ar.name'),

            ],
            'en' => [
                'name'       => $request->input('en.name'),
            ],

            'parent_id'     => $request->input('parent_id'),
        ]);
        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/services/' . $request->image->hashName()));

            $service->image ='uploads/services/' . $request->image->hashName();
        }

        $service->save();
        return response()->json(['status'=>'success','data'=>$service]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        if ($service->image != 'default.png') {
            Storage::disk('public_uploads')->delete('/services/' . $service->image);
        }
        $service->delete();
        return response()->json(['status'=>'success']);
    }
}
