<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMINHOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('guest:admin')->except('logout');;
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {

     //dd($request->all());
      $v = Validator::make($request->all(), [
            'email' => 'required|email', 
            'password' => 'required|min:3',
        ]);

        if ($v->fails()) {
          return Response::json(['errors' => $v->errors()]);
      }

        if (Auth::guard('admin')->validate(['email' => $request->email, 'password' => $request->password, 'active' => 0])) {

         session()->flash('error', __('admin.notActive'));
          return back();
        }

        $credentials  = array(
          'email' => $request->email, 
          'password' => $request->password
        );

        if (Auth::guard('admin')->attempt($credentials,$request->has('remember')))
        {
          session()->flash('success', __('admin.loginsuccessfully'));
           return redirect()->intended($this->redirectPath());
        }

          session()->flash('error', __('admin.notbase'));
           return back();
    }


    protected function guard()
    {
       return Auth::guard('admin');
    }
}
