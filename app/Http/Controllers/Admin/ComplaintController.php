<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Complaints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class ComplaintController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:complaints-read')->only(['index']);
        $this->middleware('permission:complaints-delete')->only(['destroy']);
    }
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Complaints::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($query) {
                return '<img src=' . $query->image_path . ' border="0" style=" width: 80px; height: 80px;" class="image-show img-circle" />';
            })
           
            ->addColumn('action', function($row){
    
            if (Auth::guard('admin')->user()->hasPermission('complaints-delete')){
                $btn = 
                '<form class="delete"  action="' . route("destroycomplaints", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn =  '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
    
                        return $btn;
                    })
    
                    ->rawColumns(['action','active','image'])
                    ->make(true);
    
        }
    
        return view('admin.complaints.index');
    }

    public function destroy($id)
    {
        $complaints = Complaints::findOrFail($id);

        if ($complaints->image != 'default.png') {
            Storage::disk('public_uploads')->delete('/complaints/' . $complaints->image);
        }
        
        $complaints->delete();
        return response()->json(['status'=>'success']);
    }
}
