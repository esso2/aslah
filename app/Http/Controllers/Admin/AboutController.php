<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAbout;
use App\Models\About;
use App\Models\Aboutus;
use Illuminate\Http\Request;

class AboutController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:aboutus-read')->only(['index']);
        $this->middleware('permission:aboutus-update')->only(['update']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutus = Aboutus::first();
        return view('admin.aboutus.edit', compact('aboutus'));
    }

    public function update(UpdateAbout $request)
    {
        $aboutus = Aboutus::findOrFail(1);
        $aboutus->update([
            'ar' => [
                'aboutuses'       => $request->input('ar.aboutuses'),
            ],
            'en' => [
                'aboutuses'       => $request->input('en.aboutuses'),
            ],
        ]);

        $aboutus->save();
        return response()->json(['status'=>'success','data'=>$aboutus]);
        
    }
}
