<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCity;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class CitiesController extends Controller
{


    public function __construct()
    {
        $this->middleware('permission:city-read')->only(['index']);
        $this->middleware('permission:city-create')->only(['create', 'store']);
        $this->middleware('permission:city-update')->only(['edit', 'update']);
        $this->middleware('permission:city-delete')->only(['destroy']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = City::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('city-update')){
             $btn ='<a href="' .route("cities.edit", $row->id). '" type="button" class="btn btn-info btn-icon"><i class="icofont icofont-pen-alt-4"></i></i></a> &nbsp;';
            }else{
             $btn = '<a  href="" class="btn btn-primary btn-xs disabled">Edit</i></a>';
            }
    
            if (Auth::guard('admin')->user()->hasPermission('city-delete')){
                $btn = $btn.
                '<form class="delete"  action="' . route("cities.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-icon" title=" ' . 'Delete' . ' "><i class="icofont icofont-trash"></i></button>
                    </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
    
                        return $btn;
                    })
    
                    ->rawColumns(['action'])
                    ->make(true);
    
            }
        return view('admin.cities.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCity $request)
    {
        $city = City::create([

                    'ar' => [
                        'name'       => $request->input('ar.name'),
                    ],
                    'en' => [
                        'name'       => $request->input('en.name'),
                    ]
                ]);

        $city->save();

        return response()->json(['status'=>'success','data'=>$city]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view('admin.cities.edit',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->update([
            'ar' => [
                'name'       => $request->input('ar.name'),
            ],
            'en' => [
                'name'       => $request->input('en.name'),
            ]
        ]);

        $city->save();
        return response()->json(['status'=>'success','data'=>$city]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();
        return response()->json(['status'=>'success']);
    }
}
