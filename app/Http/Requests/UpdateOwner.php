<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOwner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'string|required',
            'phone'=>'required',
            'email'=>'email|required',
            'password'=>'required',
            'commerical'=>'required',
            'bank_name'=>'required',
            'bank_account_owner'=>'required',
            'account_number'=>'required',
        ];
    }
}
