<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOwner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'string|required',
            'phone'=>'required',
            'address'=>'string|required',
            'password'=>'required',
            'min_salary'=>'required',
            'from'=>'nullable',
            'to'=>'nullable',
            // 'national_number'=>'string|required',
        ];
    }
}
