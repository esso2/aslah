<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OwnerServiceDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name_ar' => $this->translate('ar')->name,
            'name_en' => $this->translate('en')->name,
            'image' => $this->image,
            'price' =>  $this->pivot->price,
            'description' =>  $this->pivot->description,
        ];

        return $data;
    }
}
