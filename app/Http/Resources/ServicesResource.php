<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServicesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $data = [
            'id' => $this->id,
            'name_ar' => $this->translate('ar')->name,
            'name_en' => $this->translate('en')->name,
            'image' => $this->image,
        ];

        if ($request->isMethod('post')) {
            $data['price'] = $this->servprice;
            $data['descreption'] = $this->servdesc;
        }

        return $data;
    }
}
