<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OwnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        

        return [
            'id' => $this->id,
            'owner_name' => $this->name,
            'owner_image' => $this->image,
            'address' => $this->address,
            'service' => ServicesResource::make($this->services),
            'city' => CityResource::make($this->cities),
            'rate' => $this->rates()->get()->avg('rate'),
            //'phone' => $this->phone,
            //'address' => $this->address,
            //'min_salary' => $this->min_salary,
            //'services' => ServicesResource::collection($this->ownerservices),
        ];

    }
}
