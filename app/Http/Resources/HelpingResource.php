<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HelpingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'support_number' => $this->support_number,
            'support_number' => $this->support_number,
            'vedio_link' => $this->vedio_link,
            'question_ar' => $this->translate('ar')->question,
            'question_en' => $this->translate('en')->question,
        ];
    }
}
