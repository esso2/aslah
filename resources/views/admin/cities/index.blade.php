@extends('admin.layout.master')
@section('content')

        <div class="pcoded-content">
            <div class="pcoded-inner-content">
        
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="page-header">
                            <div class="page-header-title">
                                <h4> @lang('admin.cities') </h4>
                            </div>
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="">
                                            <i class="icofont icofont-home"></i>
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">@lang('admin.cities')</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">@lang('admin.cities')</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
        
                        <!-- Page body start -->
                        <div class="page-body">
                            <div class="row">
                                <div class="col-sm-10">
                                    <!-- Basic Form Inputs card start -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>@lang('admin.cities')</h5>
                                        </div>
                                        <div class="card-block">
                                            <div class="dt-responsive table-responsive">
                                                    <table  class="table table-striped table-bordered nowrap city-table">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">#</th>
                                                                <th class="text-center">@lang('admin.city')</th>
                                                                <th class="text-center"></th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                            </div>
                                    </div>
                                    <!-- Basic Form Inputs card end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    
@endsection

@push('js')
<script>
 $(function () {

    var locale = '{{ config('app.locale') }}';
    if (locale == 'ar') {
    var table = $('.city-table').DataTable({
        "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"},
        processing: true,
        serverSide: true,

        ajax: "{{ route('cities.index') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name',searchable: true, sortable : true},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        responsive:true,
        searching: true,
        order:[0,'desc']
    }); 
        }else{

    var table = $('.city-table').DataTable({
        "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json"},
        processing: true,
        serverSide: true,
        ajax: "{{ route('cities.index') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name',searchable: true, sortable : true},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        responsive:true,
        searching: true,
        order:[0,'desc']
    }); 
}  

    $('body').on('submit','#delform',function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
          url: url,
          method: "delete",
          data: {
              _token: '{{ csrf_token() }}',
          },
          success: function (response) {
              if (response.status == 'success'){
                  new Noty({
                      type: 'success',
                      layout: 'topRight',
                      text: "@lang('admin.deletesuccessfully')",
                      timeout: 5000,
                      killer: true
                  }).show();
                  table.ajax.reload();
              }
          }
        });
        })
});    
</script>   


@endpush