@extends('admin.layout.master')
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.cities') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.cities')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.addcities')</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('admin.addcities')</h5>
                                </div>
                                <div class="card-block">
                                    <form id="cities" method="POST" data-parsley-validate>
                                        @csrf

                                        @foreach (config('translatable.locales') as $locale)
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">@lang('admin.' .$locale. '.city')</label>
                                            <div class="col-sm-10">
                            <input type="text" name="{{$locale}}[name]" value="{{old($locale . '.city')}}" required parsley-trigger="change" placeholder="@lang('admin.'.$locale .'.city')" class="form-control form-control-round">
                                            </div>
                                        </div>
                                        @endforeach

                                        <div class="form-group has-danger row">
                                            <div class="col-sm-10">
                                    <button class="btn btn-info btn-round" type="submit" name="submit" style="float: right;">@lang('admin.save')</button>
                                    &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                                    <a href="{{url()->previous()}}" class="btn btn-danger btn-round" style="margin-left: 30px;">@lang('admin.cancel')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- Basic Form Inputs card end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page body end -->
        @endsection

        @push('js')
        <script>
            $(function () {
        
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
        
                    $('body').on('submit','#cities',function (e) {
                        e.preventDefault();
                        // alert('asdasd');
                        $.ajax({
                            url: '{{ route('cities.store')}}',
                            method: "post",
                            data: new FormData(this),
                            dataType: 'json',
                            cache       : false,
                            contentType : false,
                            processData : false,
        
                            success: function (response) {
                                //console.log(response);
                                if(response.status == 'success'){
                                    new Noty({
                                        type: 'success',
                                        layout: 'topRight',
                                        text: "@lang('admin.addsuccessfully')",
                                        timeout: 5000,
                                        killer: true
                                    }).show();
                                }
                            },
                        });
                    });
        
                });
        </script>
        @endpush
