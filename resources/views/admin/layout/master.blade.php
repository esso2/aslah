<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

<head>
    <title>إصلاح || Esllah</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Phoenixcoded">
    <meta name="keywords" content=" Admin ">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="essoemmo">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('AdminS/assets_ar/images/3030.png')}}" type="image/x-icon">

    @if (app()->getlocale() == 'ar')

    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300&display=swap" rel="stylesheet"> 
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/icon/themify-icons/themify-icons.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/icon/font-awesome/css/font-awesome.min.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/icon/icofont/css/icofont.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/pages/flag-icon/flag-icon.min.css')}}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/pages/menu-search/css/component.css')}}">
    <!-- Switch component css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/bower_components/switchery/css/switchery.min.css')}}">
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/bower_components/select2/css/select2.min.css')}}">
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/bower_components/multiselect/css/multi-select.css')}}">
        <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/css/style.css')}}">
    <!--color css-->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/css/linearicons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/css/simple-line-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/css/ionicons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/css/jquery.mCustomScrollbar.css')}}">
    <!-- Horizontal-Timeline css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/pages/dashboard/horizontal-timeline/css/style.css')}}">
    <!-- amchart css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/pages/dashboard/amchart/css/amchart.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_ar/pages/flag-icon/flag-icon.min.css')}}">

    @else

    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@600;700;900&display=swap" rel="stylesheet"> 
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css"
        href="{{asset('AdminS/assets_en/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/icon/themify-icons/themify-icons.css')}}">

    <link rel="stylesheet" type="text/css"
        href="{{asset('AdminS/assets_ar/icon/font-awesome/css/font-awesome.min.css')}}">

    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/icon/icofont/css/icofont.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/pages/flag-icon/flag-icon.min.css')}}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/pages/menu-search/css/component.css')}}">
         <!-- Switch component css -->
   <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/bower_components/switchery/css/switchery.min.css')}}">
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{asset('AdminS/assets_en/bower_components/select2/css/select2.min.css')}}">
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
        href="{{asset('AdminS/assets_en/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('AdminS/assets_en/bower_components/multiselect/css/multi-select.css')}}">

    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/css/style.css')}}">
    <!--color css-->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/css/linearicons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/css/simple-line-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/css/ionicons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/css/jquery.mCustomScrollbar.css')}}">
    <!-- Horizontal-Timeline css -->
    <link rel="stylesheet" type="text/css"
        href="{{asset('AdminS/assets_en/pages/dashboard/horizontal-timeline/css/style.css')}}">
    <!-- amchart css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/pages/dashboard/amchart/css/amchart.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('AdminS/assets_en/pages/flag-icon/flag-icon.min.css')}}">



    @endif

    @stack('css')

    {{--noty--}}
    <link rel="stylesheet" href="{{ asset('AdminS/noty/noty.css') }}">
    <script src="{{ asset('AdminS/noty/noty.min.js') }}"></script>

    {{--parsly--}}
    <link rel="stylesheet" href="{{ asset('AdminS/parsley/parsly.css') }}">
</head>

<body>

    <!-- Menu header start -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header" header-theme="theme4">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <a class="mobile-search morphsearch-search" href="#">
                            <i class="ti-search"></i>
                        </a>
                        <a href="{{route('adminhome')}}">
                            <img class="img-fluid" src="{{asset('AdminS/assets_ar/images/esla7.png')}}"
                                alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <div>
                            <ul class="nav-left">
                                <li>
                                    <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a>
                                    </div>
                                </li>

                            </ul>
                            <ul class="nav-right">

                                <?php $localeCodear = 'ar' ?>
                                <?php $localeCodeen = 'en' ?>

                                <li class="header-notification lng-dropdown">
                                    @if (app()->getlocale() == 'ar')
                                    <a rel="alternate" hreflang="{{ $localeCodeen }}"
                                        href="{{ LaravelLocalization::getLocalizedURL($localeCodeen, null, [], true) }}"
                                        id="dropdown-active-item">
                                        <i class="flag-icon flag-icon-ca m-r-5"></i> English
                                    </a>
                                    @else
                                    <a rel="alternate" hreflang="{{ $localeCodear }}"
                                        href="{{ LaravelLocalization::getLocalizedURL($localeCodear, null, [], true) }}"
                                        id="dropdown-active-item">
                                        <i class="flag-icon flag-icon-sa m-r-5"></i> Arabic
                                    </a>
                                    @endif
                                </li>

                                <li class="header-notification">
                                    <a href="#!">
                                        <i class="ti-bell"></i>
                                        <span class="badge">5</span>
                                    </a>
                                    <ul class="show-notification">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>

                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center"
                                                    src="{{asset('AdminS/assets_ar/images/esla7.png')}}"
                                                    alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer
                                                        elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </li>

                                <li class="user-profile header-notification">
                                    <a href="#!">
                                        <img src="{{asset('AdminS/assets_ar/images/esla7.png')}}"
                                            alt="User-Profile-Image">
                                        <span>{{auth()->user()->guard(['admin'])->name}}</span>
                                        <i class="ti-angle-down"></i>
                                    </a>
                                    <ul class="show-notification profile-notification">

                                        <li>
                                            <a href="{{ route('admin.logout') }}"
                                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                <i class="ti-layout-sidebar-left"></i>@lang('admin.logout')</a>
                                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                                                class="d-none">
                                                @csrf
                                            </form>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar" pcoded-header-position="relative">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-40" src="{{asset('AdminS/assets_ar/images/esla7.png')}}"
                                        alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span>{{auth()->user()->guard(['admin'])->name}}</span>
                                    </div>
                                </div>
                            </div>

                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-item active">
                                    <a href="{{route('adminhome')}}">
                                        <span class="pcoded-micon"><i class="fa fa-dashboard"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.dashboard')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>

                                @if(Auth::guard('admin')->user()->hasPermission('serv-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-briefcase"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.services')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('serv-create'))
                                        <li class="">
                                            <a href="{{route('services.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addservice')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('serv-read'))
                                        <li class="">
                                            <a href="{{route('services.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.services')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('advert-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-trademark"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.advertisments')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('advert-create'))
                                        <li class="">
                                            <a href="{{route('advertisments.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addadvertisment')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('advert-read'))
                                        <li class="">
                                            <a href="{{route('advertisments.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.advertisments')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('banners-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-user-secret"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.banners')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('banners-create'))
                                        <li class="">
                                            <a href="{{route('banners.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addbanners')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('banners-read'))
                                        <li class="">
                                            <a href="{{route('banners.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.banners')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('order-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-user-secret"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.orders')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('order-create'))
                                        <li class="">
                                            <a href="navbar-light.html">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addorders')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('order-read'))
                                        <li class="">
                                            <a href="navbar-dark.html">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.orders')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('package-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-globe"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.advertisingpackages')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('package-create'))
                                        <li class="">
                                            <a href="{{route('packages.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addadvertisingpackage')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif 
                                        @if(Auth::guard('admin')->user()->hasPermission('package-read'))
                                        <li class="">
                                            <a href="{{route('packages.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.advertisingpackages')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('city-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-globe"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.cities')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('city-create'))
                                        <li class="">
                                            <a href="{{route('cities.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addcities')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif 
                                        @if(Auth::guard('admin')->user()->hasPermission('city-read'))
                                        <li class="">
                                            <a href="{{route('cities.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.cities')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('trans-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-briefcase"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.transactions')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('trans-read'))
                                        <li class="">
                                            <a href="navbar-dark.html">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.transactions')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('owners-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-user-secret"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.owners')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('owners-create'))
                                        <li class="">
                                            <a href="{{route('owners.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addowners')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('owners-read'))
                                        <li class="">
                                            <a href="{{route('owners.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.owners')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('users-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-users"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.users')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('users-create'))
                                        <li class="">
                                            <a href="{{route('users.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addusers')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('users-read'))
                                        <li class="">
                                            <a href="{{route('users.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.users')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('admins-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-black-tie"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.admins')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('admins-create'))
                                        <li class="">
                                            <a href="{{route('admins.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addadmins')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif

                                        @if(Auth::guard('admin')->user()->hasPermission('admins-read'))
                                        <li class="">
                                            <a href="{{route('admins.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.admins')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('roles-read'))
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-map-signs"></i></span>
                                        <span class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">@lang('admin.roles')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        @if(Auth::guard('admin')->user()->hasPermission('roles-create'))
                                        <li class="">
                                            <a href="{{route('roles.create')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar">@lang('admin.addroles')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Auth::guard('admin')->user()->hasPermission('roles-read'))
                                        <li class="">
                                            <a href="{{route('roles.index')}}">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext"
                                                    data-i18n="nav.navigate.navbar-inverse">@lang('admin.roles')</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('aboutus-read'))
                                <li class="pcoded-item {{ URL::route('about') === URL::current() ? 'active' : '' }}">
                                    <a href="{{route('about')}}">
                                        <span class="pcoded-micon"><i class="fa fa-book"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.aboutus')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('help-read'))
                                <li class="pcoded-item {{ URL::route('helping') === URL::current() ? 'active' : '' }}">
                                    <a href="{{route('helping')}}">
                                        <span class="pcoded-micon"><i class="fa fa-gear"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.helping')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('term-read'))
                                <li class="pcoded-item {{ URL::route('terming') === URL::current() ? 'active' : '' }}">
                                    <a href="{{route('terming')}}">
                                        <span class="pcoded-micon"><i class="fa fa-gear"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.terming')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->hasPermission('propaganda-read'))
                                <li class="pcoded-item {{ URL::route('propaganding') === URL::current() ? 'active' : '' }}">
                                    <a href="{{route('propaganding')}}">
                                        <span class="pcoded-micon"><i class="fa fa-gear"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.propaganding')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                @endif

                                {{-- @if(Auth::guard('admin')->user()->hasPermission('settings-read'))
                                <li class="pcoded-item {{ URL::route('setting') === URL::current() ? 'active' : '' }}">
                                    <a href="{{route('setting')}}">
                                        <span class="pcoded-micon"><i class="fa fa-gear"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.settings')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                @endif --}}

                                @if(Auth::guard('admin')->user()->hasPermission('complaints-read'))
                                <li class="pcoded-item {{Request::is('admin/complaints/') ? 'active':''}}">
                                    <a href="{{route('complaints')}}">
                                        <span class="pcoded-micon"><i class="fa fa-warning"></i></span>
                                        <span class="pcoded-mtext">@lang('admin.complaints')</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </nav>

                    <section>

                        @yield('content')

                    </section>

                </div>
            </div>
        </div>
    </div>



    @include('admin.layout.session')

    <!-- Warning Section Ends -->

    @if (app()->getlocale() == 'ar')
    <!-- Required Jqurey -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/jquery/js/jquery.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/jquery-ui/js/jquery-ui.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/popper.js/js/popper.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/bootstrap/js/bootstrap.min.js')}}">
    </script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/modernizr/js/modernizr.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/modernizr/js/css-scrollbars.js')}}">
    </script>
    <!-- classie js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/classie/js/classie.js')}}"></script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/i18next/js/i18next.min.js')}}">
    </script>
    <script type="text/javascript"
        src="{{asset('AdminS/assets_ar/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('AdminS/assets_ar/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>

    <!-- Select 2 js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/select2/js/select2.full.min.js')}}"></script>

    <!-- Multiselect js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/pages/dashboard/custom-dashboard.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/pages/advance-elements/select2-custom.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/js/menu/menu-rtl.js')}}"></script>
    
    {{--parsly--}}
    <script src="{{asset('AdminS/parsley/parsley.min.js')}}"></script>
    <script src="{{asset('AdminS/i18n/ar.js')}}"></script>

    <!-- pcmenu js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/js/pcoded.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/js/demo-12.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_ar/js/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    {{-- <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_ar/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script> --}}
    <script src="{{asset('AdminS/assets_ar/pages/data-table/js/data-table-custom.js')}}"></script>

    <script>
        $(document).ready(function () {
            $("#pcoded").pcodedmenu({
                verticalMenuplacement: 'right',
            });
        });

    </script>

    @else

    <!-- Required Jqurey -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/jquery/js/jquery.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/jquery-ui/js/jquery-ui.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/popper.js/js/popper.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/bootstrap/js/bootstrap.min.js')}}">
    </script>
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/select2/js/select2.full.min.js')}}">
    </script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript"
        src="{{asset('AdminS/assets_en/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/modernizr/js/modernizr.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/modernizr/js/css-scrollbars.js')}}">
    </script>
    <!-- classie js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/classie/js/classie.js')}}"></script>

    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/modal.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/modalEffects.js')}}"></script>

    <script type="text/javascript" src="{{asset('AdminS/assets_en/pages/advance-elements/select2-custom.js')}}">
    </script>
    <!-- i18next.min.js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/i18next/js/i18next.min.js')}}">
    </script>
    <script type="text/javascript"
        src="{{asset('AdminS/assets_en/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('AdminS/assets_en/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}">
    </script>
    <script type="text/javascript"
        src="{{asset('AdminS/assets_en/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>

        <!-- Select 2 js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/bower_components/select2/js/select2.full.min.js')}}">
    </script>

    <!-- Multiselect js -->
    <script type="text/javascript"
        src="{{asset('AdminS/assets_en/bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js')}}">
    </script>
    <script type="text/javascript"
        src="{{asset('AdminS/assets_en/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>

    <!-- Custom js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/pages/dashboard/custom-dashboard.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/pages/advance-elements/select2-custom.js')}}">
    </script>
    {{--parsly--}}
    <script src="{{asset('AdminS/parsley/parsley.min.js')}}"></script>
    <script src="{{asset('AdminS/i18n/en.js')}}"></script>

    <!-- pcmenu js -->
    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/pcoded.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/demo-12.js')}}"></script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/jquery.mCustomScrollbar.concat.min.js')}}">
    </script>
    <script type="text/javascript" src="{{asset('AdminS/assets_en/js/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    {{-- <script src="{{asset('AdminS/assets_en/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('AdminS/assets_en/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script> --}}
    <script src="{{asset('AdminS/assets_en/pages/data-table/js/data-table-custom.js')}}"></script>


    @endif

    @stack('js')


    <script>
        // image preview

        $(".image").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-show').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }
        });

        $(".image_owner").change(function () {

if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
        $('.image_owner-show').attr('src', e.target.result);
    }

    reader.readAsDataURL(this.files[0]);
}
});

    </script>

    <script>
        //delete
        $('body').on('click', '.delete', function (e) {
            var that = $(this)
            e.preventDefault();

            var n = new Noty({
                text: "@lang('admin.deleteconfirm')",
                type: "warning",
                killer: true,
                buttons: [
                    Noty.button("@lang('admin.yes')", 'btn btn-success mr-2', function () {
                        that.closest('form').submit();
                    }),

                    Noty.button("@lang('admin.no')", 'btn btn-primary mr-2', function () {
                        n.close();
                    })
                ]
            });

            n.show();

        }); //end of delete


        // $('.select2me').select2({
        //     placeholder: "Select",
        //     width: '100% !important',
        //     allowClear: true
        // });

    </script>
</body>

</html>
