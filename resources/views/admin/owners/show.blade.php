@extends('admin.layout.master')
@section('content')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4> @lang('admin.owners') </h4>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">@lang('admin.owners')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">@lang('admin.owners')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Simple card start -->
                            <div class="card simple-cards">
                                <div class="card-header">
                                    <h5>{{$owner->name}}</h5>
                                </div>
                                <div class="card-block">
                                    <div class="row users-card">
                                        <div class="col-lg-12 col-xl-12">
                                            <div class="card user-card">
                                                <div class="card-header-img">
                                                    <img class="img-fluid img-circle"
                                                        src="{{asset('uploads/owners/' . $owner->image)}}" alt="card-img">
                                                    <h4>{{$owner->name}}</h4>
                                                    <h5>{{$owner->email}}</h5>
                                                    
                                                    @foreach ($services as $item)
                                                    <h6>{{$item->name}}</h6>
                                                    <h6>{{$item->price}}</h6>
                                                    @endforeach
                                                </div>
                                                <div class="btn-group" role="group" aria-label="user-cards">
                                                    <button type="button"
                                                        class="btn btn-float box-shadow-0 btn-outline-primary">
                                                        <span class="ladda-label">
                                                            <i class="icofont icofont-bell-alt card-icon"></i>
                                                            <span>10</span>
                                                        </span>
                                                    </button>
                                                    <button type="button"
                                                        class="btn btn-float box-shadow-0 btn-outline-primary">
                                                        <span class="ladda-label">
                                                            <i class="icofont icofont-heart card-icon"></i>
                                                            <span>15</span>
                                                        </span>
                                                    </button>
                                                    <button type="button"
                                                        class="btn btn-float box-shadow-0 btn-outline-primary">
                                                        <span class="ladda-label">
                                                            <i class="icofont icofont-bag-alt card-icon"></i>
                                                            <span>7</span>
                                                        </span>
                                                    </button>
                                                    <button type="button"
                                                        class="btn btn-float box-shadow-0 btn-outline-primary">
                                                        <span class="ladda-label">
                                                            <i class="icofont icofont-ui-message card-icon"></i>
                                                            <span>80</span>
                                                        </span>
                                                    </button>

                                                </div>
                                                <p>Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod
                                                    temp or incidi dunt ut labore et.</p>
                                                <div>
                                                    <button type="button"
                                                        class="btn btn-primary btn-outline-primary waves-effect waves-light m-r-15"><i
                                                            class="icofont icofont-plus m-r-5"></i>Follow</button>
                                                    <button type="button"
                                                        class="btn btn-success btn-outline-success waves-effect waves-light"><i
                                                            class="icofont icofont-user m-r-5"></i>Profile</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end of row -->
                                </div>
                            </div>
                            <!-- Simple card end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
