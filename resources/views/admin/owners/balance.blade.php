@extends('admin.layout.master')
@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4> @lang('admin.ownersbalances') </h4>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">@lang('admin.owners')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">@lang('admin.ownersbalances')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Simple card start -->
                            <div class="card simple-cards">

                                <div class="card">
                                    <div class="card-header">
                                        <h5>Multi-Column Ordering</h5>
                                        <button type="button" class="btn btn-success waves-effect" data-toggle="modal" data-target="#small-Modal">تحصيل</button>
                                        <div class="modal fade" id="small-Modal" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">تحصيل مبلغ</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>المبلغ المستحق</h5>
                                                        <form action="">
                                                            <div class="form-group row">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control form-control-round" name="name"
                                                                        placeholder="@lang('admin.companyname')" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group has-danger row">
                                                                <div class="col-sm-4">
                                                                    <button class="btn btn-info btn-round" type="submit" name="submit"
                                                                        style="float: right;margin-right: -100px;">@lang('admin.save')</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="multi-colum-dt" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>@lang('admin.balance')</th>
                                                        <th>@lang('admin.details')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>550</td>
                                                        <td>lkdlklsajfks</td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <label for=""><h3>Total : 550$</h3></label>
                                    </div>
                                    <!-- end of row -->
                                </div>
                            </div>
                            <!-- Simple card end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
