@extends('admin.layout.master')

@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
            <div class="page-header">
                <div class="page-header-title">
                    <h4> @lang('admin.owners') </h4>
                </div>
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">@lang('admin.owners')</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">@lang('admin.editowners')</a>
                        </li>
                    </ul>
                </div>
            </div>
      <!-- Page body start -->
      <div class="page-body">
        <div class="row">
            <div class="col-sm-8">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>@lang('admin.editowners')</h5>
                    </div>
                    <div class="card-block">
                        <form id="owners" method="POST" data-parsley-validate>
                            @csrf
                            @method('put')
                            <input type="hidden" name="OwnerId" id="owner_id" value="{{$owner->id}}"  class="form-control">

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.companyname')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="name"
                                        placeholder="@lang('admin.companyname')" value="{{$owner->name}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.companyphone')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="phone"
                                        placeholder="@lang('admin.companyphone')" value="{{$owner->phone}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.email')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="email"
                                        placeholder="@lang('admin.email')" value="{{$owner->email}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.services')</label>
                                <div class="col-sm-10">
                                <select id="hello-single" name="service_id" class="form-control form-control-round">
                                    <option value="">---</option>
                                    @foreach ($services as $service)
                                    <option value="{{$service->id}}" @if($service->id == $owner->service_id) selected @endif>{{$service->name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.commerical')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="commerical"
                                        placeholder="@lang('admin.commerical')" value="{{$owner->commerical}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.min_salary')</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control form-control-round" name="min_salary"
                                        placeholder="@lang('admin.min_salary')" value="{{$owner->min_salary}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.from')</label>
                                <div class="col-sm-10">
                                    <input type="time" class="form-control form-control-round" name="from"
                                        value="{{$owner->from}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.to ')</label>
                                <div class="col-sm-10">
                                    <input type="time" class="form-control form-control-round" name="to"
                                    value="{{$owner->to}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.bank')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="bank_name"
                                    value="{{$owner->bank_name}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.acountowner')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="bank_account_owner"
                                    value="{{$owner->bank_account_owner}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.accountnumber')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="account_number"
                                    value="{{$owner->account_number}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.password')</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" value="{{old('password')}}"  class="form-control form-control-round" placeholder="Enter Admin Password" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.address')</label>
                                <div class="col-sm-10">
                                    <textarea name="address" class="form-control form-control-round" id="" cols="10" rows="5"
                                    required>{{$owner->address}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.image')</label>
                                <input type="file" name="image" id="image" value="{{$owner->image}}" class="filestyle image" data-buttonname="btn-primary" >
                                <div class="form-group col-md-6">
                                    <img src="{{$owner->image_path}}" 
                                    width="250" height="130" alt="" 
                                    style="margin-right: 50px;" 
                                    class="image-show"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.image_owner')</label>
                                <input type="file" name="image_owner" id="image_owner" value="{{$owner->image_owner}}" class="filestyle image_owner" data-buttonname="btn-primary" >
                                <div class="form-group col-md-6">
                                    <img src="{{$owner->image_path}}" 
                                    width="250" height="130" alt="" 
                                    style="margin-right: 50px;" 
                                    class="image_owner-show"/>
                                </div>
                            </div>

                            <div class="form-group has-danger row">
                                <div class="col-sm-10">
                        <button class="btn btn-info btn-round" type="submit" name="submit" style="float: right;">@lang('admin.save')</button>
                        &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                        <a href="{{url()->previous()}}" class="btn btn-danger btn-round" style="margin-left: 30px;">@lang('admin.cancel')</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>
    </div>
    </div>
</div>

@endsection
@push('js')
<script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#owners',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                 let id = $('#owner_id').val();
                 var url = '{{ route('owners.update', ':id') }}';
                 url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if (response.errors){
                            $.each(response.errors, function( index, value ) {
                                new Noty({
                                    type: 'success',
                                    layout: 'topRight',
                                    text: value,
                                    timeout: 2000,
                                    killer: true
                                }).show();
                            });
                        }

                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.updatesuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush
