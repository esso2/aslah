@extends('admin.layout.master')
@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4> {{$user->name}} </h4>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">@lang('admin.users')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">@lang('admin.usersaddresses')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                                        <div class="card simple-cards col-sm-12">
                                        
                                            <div class="card-block">
                                                <div class="row users-card">
                                                    @foreach ($user->addresses as $item)
                                                    <div class="col-lg-3 col-xl-3">
                                                        <div class="card user-card">
                                                            <div class="card-header-img">

                                                                <h5>{{$item->nameaddress}}</h5>

                                                                <h4>{{$item->location}}</h4>
                                                            </div>
                                                          
                                                        </div>
                                                    </div>
                                                    @endforeach
                                            

                                                </div>
                                                <!-- end of row -->
                                            </div>
                                        </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Page body end -->
        @endsection
