@extends('admin.layout.master')
@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.users') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.users')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.users')</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('admin.users')</h5>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-striped table-bordered city-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">@lang('admin.name')</th>
                                                    <th class="text-center">@lang('admin.phone')</th>
                                                    <th class="text-center">@lang('admin.image')</th>
                                                    <th class="text-center">@lang('admin.active')</th>
                                                    <th class="text-center">@lang('admin.email')</th>
                                                    <th class="text-center"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- Basic Form Inputs card end -->
                            </div>
                        </div>
                    </div>

                    <div class="modal fade modal-flex" id="Modal-tab" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" id="address">
                                
                                {{-- <div class="modal-body" >
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                    <ul class="nav nav-tabs" role="tablist">

                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">Home</a>
                                        </li>

                                    </ul>
                                    <div class="tab-content modal-body">
                                        
                                        <div class="tab-pane active" id="tab-home" role="tabpanel">
                                            <h6>Home</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing lorem impus dolorsit.onsectetur adipiscing</p>
                                        </div>

                                    </div>
                                </div> --}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script>
    $(function () {

        var locale = '{{ config('
        app.locale ') }}';
        if (locale == 'ar') {
            var table = $('.city-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"
                },
                processing: true,
                serverSide: true,

                ajax: "{{ route('users.index') }}",
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'image',
                        name: 'image',
                        sortable: false
                    },
                    {
                        data: 'active',
                        name: 'active',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'email',
                        name: 'email',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                responsive: true,
                searching: true,
                order: [0, 'desc']
            });
        } else {

            var table = $('.city-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json"
                },
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.index') }}",
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'image',
                        name: 'image',
                        sortable: false
                    },
                    {
                        data: 'active',
                        name: 'active',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'email',
                        name: 'email',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                responsive: true,
                searching: true,
                order: [0, 'desc']
            });
        }

    $('body').on('click','#check',function () {
    //e.preventDefault();
    var active = $(this).prop('checked') == true ? 1 : 0; 
    var user_id = $(this).data('id');
     $.ajax({
        url:'{{ route('useractive') }}',
        type:'GET',
        data:{
            'active': active,
            'user_id': user_id
        },
         success: function (response) {
         if (response.status == 'success'){
             new Noty({
                type: 'success',
                layout: 'topRight',
                text: "@lang('admin.statuschange')",
                timeout: 5000,
                killer: true
              }).show();
            }
        }
    });
});


    $('body').on('click','.details',function () {
        // e.preventDefault();
        var user_id = $(this).data('id');
        $.ajax({
            url:'{{ route('useraddress') }}',
            type:'GET',
            data:{
                'user_id': user_id
            },
            success: function (response) {

                    var docRow = '';
                    $('#address').html('');

                    $.each(response, function(index, value){

                      docRow = '<div class="modal-body"><ul class="nav nav-tabs" role="tablist"><li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab-home" role="tab">'+value.nameaddress+'</a></li></ul><div class="tab-content modal-body"><div class="tab-pane active" id="tab-home" role="tabpanel"><h6>'+value.nameaddress+'</h6><p>'+value.location+'</p></div></div></div>';

                    $('#address').append(docRow);

                       });
            }
        });
    });


        $('body').on('submit', '#delform', function (e) {
            e.preventDefault();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                method: "delete",
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function (response) {
                    if (response.status == 'success') {
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "@lang('users.deletesuccessfully')",
                            timeout: 5000,
                            killer: true
                        }).show();
                        table.ajax.reload();
                    }
                }
            });
        })


    });

</script>


@endpush
