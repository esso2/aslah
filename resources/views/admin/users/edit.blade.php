@extends('admin.layout.master')
@section('content')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.users') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.users')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.editusers')</a>
                            </li>
                        </ul>
                    </div>
                </div>
      <!-- Page body start -->
      <div class="page-body">
        <div class="row">
            <div class="col-sm-10">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>@lang('admin.editusers')</h5>
                    </div>
                    <div class="card-block">
                        {{-- <h4 class="sub-title">@lang('admin.editusers')</h4> --}}
                        <form id="users" method="POST" enctype="multipart/form-data" data-parsley-validate>
                            @csrf
                            @method('put')
                            <input type="hidden" name="UserId" id="user_id" value="{{$user->id}}"  class="form-control">

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.name')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="name" value="{{$user->name}}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.phone')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-round" name="phone" value="{{$user->phone}}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.email')</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control form-control-round" name="email" value="{{$user->email}}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.password')</label>
                                <div class="col-sm-10">
                                    <input type="password" placeholder="@lang('admin.password')" class="form-control form-control-round" name="password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" style="margin-top: 81px">@lang('admin.image') :</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image" id="image" value="{{$user->image}}"  class="filestyle image" data-buttonname="btn-primary" >
                                    <img src="{{$user->image_path}}" 
                                    width="340" height="200" alt="" 
                                    style="margin-right: 70px;border-radius: 15px;"
                                    class="image-show"/>
                                </div>
                            </div>
                            
                            <div class="form-group has-danger row">
                                <div class="col-sm-10">
                        <button class="btn btn-info btn-round" type="submit" name="submit" style="float: right;margin-right: -100px;">@lang('admin.save')</button>
                        <a href="{{url()->previous()}}" class="btn btn-danger btn-round" style="margin-left: 30px;">@lang('admin.cancel')</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>
     </div>
    </div>
</div>
    <!-- Page body end -->
@endsection


@push('js')
<script>
    $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#users',function (e) {
                        e.preventDefault();
                         //alert('asdasd');
                         let id = $('#user_id').val();
                         var url = '{{ route('users.update', ':id') }}';
                         url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            method: "post",
                            data: new FormData(this),
                            dataType: 'json',
                            cache       : false,
                            contentType : false,
                            processData : false,

                    success: function (response) {
                        //console.log(response);
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('user.addsuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },
                });
            });

        });
 </script>
@endpush