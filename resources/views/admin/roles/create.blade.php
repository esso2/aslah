@extends('admin.layout.master')

@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.dashboard') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.roles')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.addroles')</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('admin.addroles')</h5>
                                </div>
                                <div class="card-block">
                                    <form action="{{route('roles.store')}}" method="post" data-parsley-validate>
                                        @csrf
                                        @method('post')

                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">@lang('admin.namerole')</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" value="{{old('name')}}" required
                                                    parsley-trigger="change" placeholder="@lang('admin.namerole')"
                                                    class="form-control form-control-round">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th style="width: 10px">#</th>
                                                        <th>@lang('admin.model')</th>
                                                        <th>@lang('admin.permissions')</th>
                                                    </tr>
                                                    @foreach($models as $index=>$model)
                                                    @if($model == 'settings')
                                                    <?php $actions = ['read', 'update']; ?>
                                                    @endif
                                                    @if($model == 'contactus')
                                                    <?php $actions = ['read', 'delete']; ?>
                                                    @endif
                                                    <tr>
                                                        <td style="width:5%">{{$index+1}}</td>
                                                        <td style="width:5%">@lang('admin.'.$model)</td>
                                                        <td class="col-sm-8 col-xl-4 m-b-30">
                                                            <select class="js-example-placeholder-multiple col-sm-8"
                                                                name="permissions[]" multiple="multiple">
                                                                @foreach($actions as $index=>$action)
                                                                <option value="{{$model.'-'.$action}}">
                                                                    @lang('admin.'.$action)</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="form-group has-danger row">
                                            <div class="col-sm-10">
                                    <button class="btn btn-info btn-round" type="submit" name="submit" style="float: right;">@lang('admin.save')</button>
                                    &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                                    <a href="{{url()->previous()}}" class="btn btn-danger btn-round" style="margin-left: 30px;">@lang('admin.cancel')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- Basic Form Inputs card end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

