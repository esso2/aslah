@extends('admin.layout.master')

@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.services') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.services')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.addservice')</a>
                            </li>
                        </ul>
                    </div>
                </div>
      <!-- Page body start -->
      <div class="page-body">
          
        <div class="row">
            
            <div class="col-sm-8">
                <!-- Basic Form Inputs card start -->
                
                <div class="card">
                    
                    <div class="card-header">
                        <h5>@lang('admin.services')</h5>
                    </div>
                    <div class="card-block">
                        <h4 class="sub-title">@lang('admin.services')</h4>
                        <form  id="services" method="POST" data-parsley-validate>
                            @csrf
                            @method('put')

                            <input type="hidden" name="serviceId" id="service_id" value="{{$service->id}}" class="form-control">

                            @foreach (config('translatable.locales') as $locale)
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.' .$locale. '.service')</label>
                                <div class="col-sm-10">
                        <input type="text" name="{{$locale}}[name]" value="{{$service->translate($locale)->name}}" required parsley-trigger="change" placeholder="@lang('admin.'.$locale .'.service')" class="form-control form-control-round">
                                </div>
                            </div>
                            @endforeach
                        
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">@lang('admin.role')</label>
                                <div class="col-sm-10">
                                    <select class="form-control form-control-round" name="parent_id" id="parent_id" required>
                                     {{-- <option value="">---</option> --}}
                                       @foreach($services as $ser)
                                       <option  value="{{$ser->id}}" @if($ser->id == $service->parent_id) selected @endif>{{$ser->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" style="margin-top: 81px">@lang('admin.image') :</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image" id="image" value="{{$service->image}}"  class="filestyle image" data-buttonname="btn-primary" >
                                    <img src="{{$service->image_path}}" 
                                    width="340" height="200" alt="" 
                                    style="margin-right: 70px;border-radius: 15px;"
                                    class="image-show"/>
                                </div>
                            </div>

                            <div class="form-group has-danger row">
                                <div class="col-sm-10">
                        <button class="btn btn-info btn-round" type="submit" name="submit" style="float: right;">@lang('admin.save')</button>
                        &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                        <a href="{{url()->previous()}}" class="btn btn-danger btn-round" style="margin-left: 30px;">@lang('admin.cancel')</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>
    </div>
    </div>
</div>

@endsection

@push('js')
<script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#services',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                 let id = $('#service_id').val();
                 var url = '{{ route('services.update', ':id') }}';
                 url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if (response.errors){
                            $.each(response.errors, function( index, value ) {
                                new Noty({
                                    type: 'success',
                                    layout: 'topRight',
                                    text: value,
                                    timeout: 2000,
                                    killer: true
                                }).show();
                            });
                        }

                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.updatesuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush