@extends('admin.layout.master')
@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.updatepropaganding') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.propaganding')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.updatepropaganding')</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <!-- Basic Form Inputs card start -->
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('admin.updatepropaganding')</h5>
                                </div>
                                <div class="card-block">
                                    <form  id="propaganding" method="POST" data-parsley-validate>
                                        @csrf
                                        @method('post')

                                        @foreach (config('translatable.locales') as $locale)
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">@lang('admin.' .$locale. '.propaganding')</label>
                                            <div class="col-sm-10">
                                        <textarea name="{{$locale}}[propagandas]" class="form-control form-control-round" cols="30" rows="10">{{$propaganding->translate($locale)->propagandas}}</textarea>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="form-group has-danger row">
                                            <div class="col-sm-10">
                                    <button class="btn btn-info btn-round" type="submit" name="submit" style="float: right;">@lang('admin.save')</button>
                                    &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
                                    <a href="{{url()->previous()}}" class="btn btn-danger btn-round" style="margin-left: 30px;">@lang('admin.cancel')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- Basic Form Inputs card end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- Page body end -->
        @endsection

        @push('js')
        <script>
            $(function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
        
                    $('body').on('submit','#propaganding',function (e) {
                        e.preventDefault();
                        //  alert('asdasd');
                         let id = $('#term_id').val();
                         var url = '{{ route('updatepropaganding', ':id') }}';
                         url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            method: "post",
                            data: new FormData(this),
                            dataType: 'json',
                            cache       : false,
                            contentType : false,
                            processData : false,
        
                            success: function (response) {
                                //console.log(response);
                                if (response.errors){
                                    $.each(response.errors, function( index, value ) {
                                        new Noty({
                                            type: 'success',
                                            layout: 'topRight',
                                            text: value,
                                            timeout: 2000,
                                            killer: true
                                        }).show();
                                    });
                                }
                                if(response.status == 'success'){
                                    new Noty({
                                        type: 'success',
                                        layout: 'topRight',
                                        text: "@lang('admin.updatesuccessfully')",
                                        timeout: 5000,
                                        killer: true
                                    }).show();
                                }
                            },
        
                        });
                    });
        
                });
         </script>
        @endpush
