@extends('admin.layout.master')
@section('content')

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="page-header-title">
                        <h4> @lang('admin.banners') </h4>
                    </div>
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.banners')</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">@lang('admin.banners')</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('admin.banners')</h5>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-striped table-bordered banner-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">@lang('admin.url')</th>
                                                    <th class="text-center">@lang('admin.image')</th>
                                                    <th class="text-center">@lang('admin.active')</th>
                                                    <th class="text-center"></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script>
    $(function () {

        var locale = '{{ config('
        app.locale ') }}';
        if (locale == 'ar') {
            var table = $('.banner-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"
                },
                processing: true,
                serverSide: true,

                ajax: "{{ route('banners.index') }}",
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'banner_url',
                        name: 'banner_url',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'image',
                        name: 'image',
                        sortable: false
                    },
                    {
                        data: 'active',
                        name: 'active',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                responsive: true,
                searching: true,
                order: [0, 'desc']
            });
        } else {

            var table = $('.banner-table').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json"
                },
                processing: true,
                serverSide: true,
                ajax: "{{ route('banners.index') }}",
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'banner_url',
                        name: 'banner_url',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'image',
                        name: 'image',
                        sortable: false
                    },
                    {
                        data: 'active',
                        name: 'active',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                responsive: true,
                searching: true,
                order: [0, 'desc']
            });
        }

    $('body').on('click','#check',function () {
    //e.preventDefault();
    var active = $(this).prop('checked') == true ? 1 : 0; 
    var banner_id = $(this).data('id');
     $.ajax({
        url:'{{ route('banneractive') }}',
        type:'GET',
        data:{
            'active': active,
            'banner_id': banner_id
        },
         success: function (response) {
         if (response.status == 'success'){
             new Noty({
                type: 'success',
                layout: 'topRight',
                text: "@lang('admin.statuschange')",
                timeout: 5000,
                killer: true
              }).show();
            }
        }
    });
});


        $('body').on('submit', '#delform', function (e) {
            e.preventDefault();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                method: "delete",
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function (response) {
                    if (response.status == 'success') {
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "@lang('admin.deletesuccessfully')",
                            timeout: 5000,
                            killer: true
                        }).show();
                        table.ajax.reload();
                    }
                }
            });
        })
    });

</script>


@endpush
