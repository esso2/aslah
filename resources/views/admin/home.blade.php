@extends('admin.layout.master')

@section('content')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">

            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="page-header-title">
                            <h4> @lang('admin.dashboard') </h4>
                        </div>
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="index.html">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('adminhome')}}">@lang('admin.dashboard')</a>
                                </li>
                                <li class="breadcrumb-item"><a href="">@lang('admin.home')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="page-body">
                        <div class="row">
                            <!-- Documents card start -->
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks dark-info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.users')}} : </h5>
                                        <ul>
                                            <li>
                                                {{-- <i class="icofont-ui-user-group"></i> --}}
                                                <i class="icofont icofont-ui-user-group text-info"></i>
                                            </li>
                                            <li class="text-right">
                                                {{\App\Models\User::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Documents card end -->
                            <!-- New clients card start -->
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.owners')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-ui-user-group text-info"></i>
                                            </li>
                                            <li class="text-right text-warning">
                                                {{\App\Models\Owner::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- New clients card end -->
                            <!-- New files card start -->
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.cities')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-earth"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\City::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.advertisingpackages')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-listing-number"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Advertisingpackage::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.banners')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-image"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Banner::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.mainservices')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-repair"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Service::where('parent_id',null)->count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.subservices')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-repair"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Service::where('parent_id',!null)->count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.advertiser')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-audio"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Advertisment::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.roles')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-check-circled"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Role::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-12">
                                <div class="card client-blocks info-border">
                                    <div class="card-block">
                                        <h5>{{__('admin.admins')}} : </h5>
                                        <ul>
                                            <li>
                                                <i class="icofont icofont-briefcase"></i>
                                            </li>
                                            <li class="text-right text-info">
                                                {{\App\Models\Admin::count()}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
