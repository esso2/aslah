<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang=en> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang=en> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang=en> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang=en>

<head>
    <meta name="google-site-verification" content="FUrYkI6Y68wKkyaZAmv32rTw6i-5Q811NsfSA_3kkoA" />
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge" />
    <meta name=author content="DSAThemes" />
    <meta name=description content="NextApp - Mobile App Landing Page Template" />
    <meta name=keywords
        content="Responsive, HTML5 template, DSAThemes, Mobile, Application, One Page, Landing, Mobile App">
    <meta name=viewport content="width=device-width, initial-scale=1.0" />
    <meta name="google-site-verification" content="gjt5uX9FMrM7MP4CVrnxuD2-LwvTonyVWQe0ivP5EAs" />
    <title>إصلاح</title>
    <link rel="shortcut icon" href=images/favicon.ico type=image/x-icon>
    <link rel=icon href=images/favicon.ico type=image/x-icon>
    <link rel=apple-touch-icon sizes=152x152 href="{{ asset('mahkmaApp/images/apple-touch-icon-152x152.png') }}">
    <link rel=apple-touch-icon sizes=120x120 href="{{ asset('mahkmaApp/images/apple-touch-icon-120x120.png') }}">
    <link rel=apple-touch-icon sizes=76x76 href="{{ asset('mahkmaApp/images/apple-touch-icon-76x76.png') }}">
    <link rel=apple-touch-icon href="{{ asset('mahkmaApp/ttt/30.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel=stylesheet>
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:300,400,500,700,900" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/bootstrap.min.css') }}" rel=stylesheet>

    <link href="{{ asset('mahkmaApp/css/fontawesome.min.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/flaticon.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/magnific-popup.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/owl.carousel.min.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/owl.theme.default.min.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/slick.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/slick-theme.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/animate.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/spinner.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/style.css') }}" rel=stylesheet>
    <link href="{{ asset('mahkmaApp/css/responsive.css') }}" rel=stylesheet>
</head>

<body>
    <div id=loader-wrapper>
        <div id=loader>
            <div class=cssload-spinner></div>
        </div>
    </div>
    <div id=page class=page>
        <header id=header class=header>
            <nav class="navbar fixed-top navbar-expand-md hover-menu navbar-dark bg-tra white-scroll">
                <div class=container>
                    <a href=#hero-1 style="color:black;" class="navbar-brand logo-black"></a>
                    <a href=#hero-1 class="navbar-brand logo-white"> <img
                            src="{{ asset('mahkmaApp/ttt/logo1.png') }}" style="width: 200px ; height: 200px"
                            alt="إصلاح" title="إصلاح"></a>
                    <button class=navbar-toggler type=button data-toggle=collapse data-target=#navbarContent
                        aria-controls=navbarContent aria-expanded=false aria-label="Toggle navigation">
                        <span class=navbar-bar-icon><i class="fas fa-bars"></i></span>
                    </button>
                    <div id=navbarContent class="collapse navbar-collapse">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item nl-simple"><a class=nav-link href=#screens-1>صور من التطبيق </a></li>

                        </ul>

                    </div>
                </div>
            </nav>
        </header>
        <section id=hero-1 class="bg-scroll hero-section division">
            <div class=container>
                <div class="row d-flex align-items-center">
                    <div class=col-md-6>
                        <div class="hero-txt mb-40 white-color wow fadeInUp" data-wow-delay=0.3s>
                            <h2 class=h2-xs>تطبيق إصلاح </h2>
                            <p class=p-md>تطبيق اصلاح هو التطبيق الامثل في المملكة العربية السعودية للحصول على الخدمات المقدمة بأعلى جوده وأفضل سعر .. واذا كنت مقدم خدمة يمكنك التسجيل على التطبيق كمقدم خدمة للبدء في تلقي طلبات الخدمات</p>
                            <div class=stores-badge>
                                <a href="#" class=store>
                                    <img class=appstore-white
                                        src={{ asset('mahkmaApp/images/store_badges/appstore-tra-white.png') }}
                                        alt=appstore-logo>
                                </a>
                                <a href="#" class=store>
                                    <img class=googleplay-white
                                        src={{ asset('mahkmaApp/images/store_badges/googleplay-tra-white.png') }}
                                        alt=googleplay-logo>
                                </a>
                                <span class=os-version>* Available on iPhone, iPad and all Android devices from
                                    5.5</span>
                            </div>
                        </div>
                    </div>
                    <div class=col-md-6>
                        <div class="hero-1-img text-center mb-40 wow fadeInLeft" data-wow-duration=1.5s
                            data-wow-delay=0.8s>
                            <img class=img-fluid src="{{ asset('mahkmaApp/ttt/ooo.png') }}"
                                alt=MANDONAK-image />
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id=screens-1 class="bg-lightgrey wide-100 screens-section division">
            <div class=container>
                <div class=row>
                    <div class="col-lg-10 offset-lg-1 section-title">
                        <h3 class=h3-lg>واجهة مستخدم بسيطة وجميلة</h3>

                    </div>
                </div>
            </div>
            <div class=screenshots-wrap>
                <div class=screens-carousel>
                    <div class="carousel-item">
                        <img src="{{ asset('mahkmaApp/ttt/1.png') }}" alt=screenshot>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('mahkmaApp/ttt/2.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/3.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/4.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/5.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/6.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/7.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/8.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/9.png') }}" alt=screenshot>
                    </div>
                    <div class=carousel-item>
                        <img src="{{ asset('mahkmaApp/ttt/10.png') }}" alt=screenshot>
                    </div>
                </div>
            </div>
        </section>


        <footer id=footer-1 class="wide-50 footer division">
            <div class=bottom-footer>
                <div class=row>
                    <div class=col-md-12>
                        <div style="text-align:center;" class=footer-copyright>
                            <p class=p-sm><a href="https://kw4s.com/"> 2021 تصميم شركة سوفت استيبس </a></p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </footer>
    </div>
    <script src="{{ asset('mahkmaApp/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/fontawesome.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.easing.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.scrollto.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/slick.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/contact-form.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/quick-form.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/comment-form.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/wow.js') }}"></script>
    <script src="{{ asset('mahkmaApp/js/custom.min.js') }}"></script>
    <script>
        new WOW().init();
    </script>
    <!-- [if lt IE 9]>
<script src="{{ asset('js/html5shiv.js') }}" type=text/javascript></script>
<script src="{{ asset('js/respond.min.js') }} " type=text/javascript></script>
<![endif] -->
</body>

</html>
