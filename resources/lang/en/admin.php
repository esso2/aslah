<?php

return [

    'dashboard' => 'Dashboard',
    'home' => 'Home',
    'login' => 'Sign In',
    'logout' => 'Logout',
    'management' => 'To Dashboard',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'confirmpassword'=>'Confirm Password',
    'remember' => 'Remember Me',
    'addsuccessfully' => 'Added Successfully',
    'updatesuccessfully' => 'Updated Successfully',
    'deletesuccessfully' => 'Delete Successfully',
    'uplodsuccessfully' => 'Image Uploded Successfully',
    'notuplodsuccessfully' => 'Image Uploded Failed .. Try Again !!',
    'notdeletesuccessfully' => 'Delete Failed .. Try Again !!',
    'deleteconfirm' => 'Delete Confirm ?',
    'notActive' => 'You Are Not Active !!',
    'loginsuccessfully' => 'You are Login Successfully',
    'notbase' => 'You Are Not In Our Base',
    'statuschange' => 'The Status Had Changed',
    'add' => 'Add',
    'edit' => 'Edit',
    'save' => 'Save',
    'next' => 'Next',
    'previous' => 'Previous',
    'cancel' => 'Cancel',
    'yes' => 'Yes',
    'no' => 'No',
    'maininfo' => 'Main Information',
    'details' => 'Details',
    'images' => 'Images',
    'features' => 'Features',
    'filters' => 'Filters',
    'unitcode' =>'Unit Code',
    'check_in'=>' Avilable From ',
    'check_out'=>' Avilable To ',
    'min_salary'=>'Min Salary',
    'national_number'=>'National Number',
    //helping page
    'helping' => 'Helping',
    'updatehelping' => 'Update Helping',
    'vedio_link'=>'Vedio Link',
    'support_number'=>'Support Number',


    //setting page
    'settings' => 'Settings',
    'updatesetting' => 'Update Setting',
    'webtitle' => 'Website Title',
    'logo' => 'Website LOGO',
    'facebook' => 'Facebook',
    'instagram' => 'Instagram',
    'linkedin' => 'Linked In',
    'twitter' => 'Twitter', 
    'instgram' => 'Instgram', 
    'phone' => 'Phone', 
    'whatsapp' => 'Whats app', 
    'website' => 'Website',
    'address' => 'Address',

    // complaints 
    'complaints' => 'Complaints',
    'type' => 'Type',
    'message' => 'Message',


    // Account statements  
    'bankaccount' => 'Bank Account',
    'addbankaccount' => 'Add Bank Account',
    'editbankaccount' => 'Edit Bank Account',

    // cities  
    'cities' => 'Cities',
    'addcities' => 'Add Cities',
    'editcities' => 'Edit Cities',

    // roles
    'roles' => 'Roles',
    'role' => 'Role',
    'addroles' =>'Add Roles',
    'editroles' =>'Edit Roles',
    'namerole' =>'Role Name',
    'model' =>'Model',
    'permissions' =>'Permissions',
    'users' =>'Users',
    'read' =>'Read',
    'create' =>'Create',
    'update' =>'Update',
    'delete' =>'Delete',

    //admins
    'admins' =>'Admins',
    'admin' =>'Admin',
    'addadmins' =>'Add Admins',
    'editadmins' =>'Edit Admins',
    'active' => 'Active',

    //users
    'usersaddresses' =>'Users Addresses',
    'users' =>'Users',
    'addusers' =>'Add Users',
    'editusers' =>'Edit Users',
    'imageprofile' =>'Profile Image',

    //owners
    'owners' =>'Owners',
    'image_owner' =>'Owner Image',
    'addowners' =>'Add Owners',
    'editowners' =>'Edit Owners',
    'active' => 'Active',
    'imageprofile' =>'Profile Image',
    'companyname' => 'Company Name',
    'from' => ' Available From :',
    'to ' => 'Available To :',
    'commerical' => 'Commerical',
    'companyphone' => 'Company Phone',
    
    'bank' => 'Bank Name',
    'acountowner' => 'Account Name',
    'accountnumber' => 'Account Number',

    //advertisment
    'advertisments' =>'Advertisments',
    'advertisment' =>'Advertisment',
    'addadvertisment' =>'Add Advertisment',
    'editadvertisment' =>'Edit Advertisment',

    //advert

    'adverts' =>'Adverts',
    'addadverts' =>'Add Advertiser',
    'editadverts' =>'Edit Advertiser',
    'advertiser' =>'Advertisers',
    'advertperiod' =>'AdvertisDuration',


    //service
    'priceservice' =>'Service Price',
    'services' =>'Services',
    'service' =>'Service',
    'addservice' =>'Add Service',
    'editservice' =>'Edit Service',
    'maincat' => 'Main Service',
    'mainservices' => 'Main Services',
    'subservices' => 'Sub Services',
    'parent_id' => 'Section',

     //orders
    'orders' =>'Orders',
    'order' =>'Order',
    'addorder' =>'Add Order',
    'editorder' =>'Edit Order',
    'order_id' =>'Order Number',


    //contactus
    'contactus' =>'Contact Us',
    'massege' =>'Massege',

     //aboutus
    'aboutus' =>'About Us',
    'addaboutus' =>'Add About Us',
    'editaboutus' =>'Edit About Us',
    'image' =>'Image',
    'title' =>'Title',
    'description' =>'Description',

    //terms
    'terms' =>'Terms & Conditions',
    'addterms' =>'Add Term',
    'editterms' =>'Edit Term',
    'termtitle' =>'Title',

    //terms
    'terms' =>'Terms & Conditions',
    'addterms' =>'Add Term',
    'editterms' =>'Edit Term',
    'termtitle' =>'Title',

    //category
    'categories' =>'Categories',
    'category' =>'Category',
    'addcategory' =>'Add Category',
    'editcategory' =>'Edit Category',
    
    //banners
    'banners' =>'Banners',
    'banner' =>'Banner',
    'addbanners' =>'Add Banner',
    'editbanners' =>'Edit Banner',
    'url' =>'Url',

    // cities  
    'cities' => 'Cities',
    'city' => 'City',
    'addcities' => 'Add City',
    'editcities' => 'Edit City',

    // advertisingpackages  
    'advertisingpackages' => 'Advertising Packages',
    'advertisingpackage' => 'Advertising Package',
    'addadvertisingpackage' => 'Add Advertising Package',
    'editadvertisingpackages' => 'Edit Advertising Package',
    'pricepackages' => 'Advertising Package Prices',
    'price' => 'Package Price',
    'period' => 'Package Period',



    //terming
    'terming' =>'Terms',
    'updateterming' =>'Terms',

    'propaganding' =>'Propaganda',
    'updatepropaganding' =>'Propaganda',

    //reserves
    'reserves' =>'Reserves',
    'addreserves' =>'Add Reserve',
    'editreserves' =>'Edit Reserve',
    'resAdmin' =>'Reservation By Admin',
    'resUser' =>'Reservation By User',
    'resWating' =>'Not Confirmed Reservation',
    'reserday' =>'Reservation From',
    'to' =>'To',

    'ar' => [
        'title' => 'Title In Arabic',
        'address' => 'Address In Arabic',
        'description' => 'Description In Arabic',
        'termtitle' =>'Title In Arabic',
        'feattitle' =>'Feature In Arabic',
        'filter' =>'Filter In Arabic',
        'city' =>'City In Arabic',
        'service' =>'Service In Arabic',
        'helping' =>'Helping In Arabic',
        'terming' =>'Terms In Arabic',
        'propaganding' =>'propaganda In Arabic',
        'service_description' =>'Service Description Arabic',
        'aboutus' =>'Aboutus Arabic',


    ],

    'en' => [
        'title' => 'Title In English',
        'address' => 'Address In English',
        'description' => 'Description In English',
        'termtitle' =>'Title In English',
        'feattitle' =>'Feature In English',
        'city' =>'City In English',
        'service' =>'Service In English',
        'helping' =>'Helping In English',
        'terming' =>'Terms In English',
        'propaganding' =>'propaganda In English',
        'service_description' =>'Service Description English',
        'aboutus' =>'Aboutus English',

    ]
];