<?php

return [

    'dashboard' => 'الرئيسية',
    'home' => 'رئيسية',
    'login' => 'تسجيل الدخول',
    'logout' => 'تسجيل خروج',
    'management' => 'لوحة التحكم',
    'name' => 'الاسم',
    'lastname' => 'الاسم الاخير',
    'email' => 'الايميل',
    'password' => 'كلمه المرور',
    'remember' => 'تذكرني',
    'addsuccessfully' => 'تم الاضافه بنجاح',
    'updatesuccessfully' => 'تم التعديل بنجاح',
    'uplodsuccessfully' => 'تم رفع الصورة بنجاح',
    'notuplodsuccessfully' => 'لم يتم رفع الصورة .. حاول مرة اخرى',
    'deletesuccessfully' => 'تم الحذف بنجاح',
    'notdeletesuccessfully' => 'لم يتم الحذف .. حاول مرة اخرى',
    'deleteconfirm' => 'هل تريد تاكيد الحذف ؟',
    'notActive' => 'انت لست مفعل !!',
    'loginsuccessfully' => 'تم تسجيل الدخول بنجاح',
    'notbase' => 'انت غير موجود فى قاعده بيناتنا',
    'statuschange' => 'تم تغير الحالة بنجاح',
    'add' => 'اضافه',
    'edit' => 'تعديل',
    'save' => 'حفظ',
    'next' => 'التالى',
    'previous' => 'السابق',
    'cancel' => 'الغاء',
    'yes' => 'نعم',
    'no' => 'لا',
    'maininfo' => 'المعلومات الرئيسيه',
    'details' => 'التفاصيل',
    'images' => 'الصور',
    'features' => 'المميزات',
    'filters' => 'الفلاتر',
    'address'=>'العنوان',
    'password'=>'الرقم السرى',
    'confirmpassword'=>'تاكيد الرقم السرى',
    'check_in'=>'متاح من ',
    'check_out'=>'متاح الى ',
    'min_salary'=>'الحد الادنى للسعر',
    'national_number'=>'الرقم القومى',

    //setting page
    'settings' => 'الاعدادات',
    'updatesetting' => 'تحديث الاعدادات',
    'webtitle' => 'اسم الموقع',
    'logo' => 'لوجو الموقع',
    'facebook' => 'فيسبوك',
    'instagram' => 'انستقرام',
    'linkedin' => 'لينكد ان',
    'twitter' => 'تويتر', 
    'instgram' => 'انتسغرام', 
    'lat' =>'خط العرض',
    'long' =>'خط الطول',
    'phone' => 'الهاتف', 
    'whatsapp' => 'واتس اب', 
    'website' => 'الموقع',  
    'addressar' => 'العنوان باللغه العربيه', 
    'addressen' => 'العنوان باللغه الانجليزيه', 

    // complaints 
    'complaints' => 'الشكاوى',
    'type' => 'النوع',
    'message' => 'الرسالة',

    // Account statements 
    'Account statements ' => 'Account Statements ',

    //roles
    'roles' => 'الصلاحيات',
    'role' => 'الصلاحية',
    'addroles' =>'اضافة الصلاحيات',
    'editroles' =>'تعديل الصلاحيات',
    'namerole' =>'اسم الصلاحية',
    'model' =>'القسم',
    'permissions' =>'الوظيفة',
    'admins' =>'المستخدمين',
    'users' =>'العملاء',
    'read' =>'عرض',
    'create' =>'اضافة',
    'update' =>'تعديل',
    'delete' =>'حذف',

    //admins
    'addadmins' =>' اضافة مستخدمين',
    'admin' =>'المستخدم',
    'editadmins' =>'تعديل مستخدمين',
    'active' => 'الحالة',

    // cities  
    'cities' => 'المدن',
    'city' => 'مدينة',
    'addcities' => 'إضافة مدينة',
    'editcities' => 'تعديل مدينة',

    
    // advertisingpackages  
    'advertisingpackages' => 'باقات الإعلانات',
    'advertisingpackage' => 'باقة إعلان',
    'addadvertisingpackage' => 'إضافة باقة إعلان',
    'editadvertisingpackages' => 'تعديل الباقات الإعلانية',
    'pricepackages' => 'أسعار الباقات الإعلانية',
    'price' => 'سعر الباقة',
    'period' => 'مدة الباقة الاعلانية',

    //users
    'usersaddresses' =>'عناوين المستخدمين',
    'users' =>'العملاء',
    'addusers' =>' اضافة عملاء',
    'editusers' =>'تعديل عملاء',
    'active' => 'الحالة',
    'imageprofile' =>'الصورة الشخصية',

    //owners
    'owners' =>'المقدمين',
    'image_owner' =>'صورة مقدم الخدمة',
    'addowners' =>' اضافة مقدم',
    'editowners' =>'تعديل مقدم',
    'active' => 'الحالة',
    'imageprofile' =>'الصورة الشخصية',
    'companyname' => 'اسم الشركة',
    'from' => 'متاح من :',
    'to ' => 'متاح الى :',
    'commerical' => 'السجل التجارى',
    'companyphone' => 'تليفون الشركة',

    'bank' => 'اسم البنك',
    'acountowner' => 'اسم مالك الحساب',
    'accountnumber' => 'رقم الحساب البنكى',

    //contactus
    'contactus' =>'رسائل الاتصال',
    'massege' =>'الرسالة',

    //aboutus
    'aboutus' =>'من نحن',
    'addaboutus' =>'اضافه من نحن',
    'editaboutus' =>'تعديل من نحن',
    'image' =>'الصورة',
    'title' =>'الاسم',
    'description' =>'الوصف',
    
    //terms
    'terms' =>'الشروط والاحكام',
    'addterms' =>'اضافه الشروط والاحكام',
    'editterms' =>'تعديل الشروط والاحكام',
    'termtitle' =>'العنوان',

    //category
    'categories' =>'الاقسام',
    'category' =>'القسم',
    'addcategory' =>'اضافة قسم',
    'editcategory' =>'تعديل قسم',
    
    //banners
    'banners' =>'البنرات',
    'banner' =>'البنر',
    'addbanners' =>'اضافة بنر',
    'editbanners' =>'تعديل بنر',
    'url' =>'الرابط',

    //country
    'countries' =>'المناطق',
    'country' =>'المنطقة',
    'addcountry' =>'اضافة منطقة',
    'editcountry' =>'تعديل منطقة',

    //advertisment
    'advertisments' =>'الاعلانات',
    'advertisment' =>'اعلان',
    'addadvertisment' =>'اضافة اعلان',
    'editadvertisment' =>'تعديل اعلان',
  
      //advert

      'adverts' =>'الاعلانات',
      'addadverts' =>'إضافة معلن',
      'editadverts' =>'تعديل معلن',
      'advertiser' =>'المعلنين',
      'advertperiod' =>'مدة الاعلان',

    //services

    'priceservice' =>'سعر الخدمة',
    'services' =>'الخدمات',
    'service' =>'خدمة',
    'addservice' =>'اضافة خدمة',
    'editservice' =>'تعديل خدمة',
    'maincat' => 'خدمة رئيسية',
    'mainservices' => 'الخدمات الرئيسية',
    'subservices' => 'الخدمات الفرعية',
    'parent_id' => 'القسم',

    
    //terming
    'terming' =>'الشروط والاحكام',
    'updateterming' =>' الشروط والاحكام',

    'propaganding' =>'شروط الاعلان',
    'updatepropaganding' =>'  شروط الاعلان',

    //orders
    'orders' =>'الطلبات',
    'order' =>'طلب',
    'addorder' =>'اضافة طلب',
    'editorder' =>'تعديل طلب',
    'order_id' =>'رقم الطلب',
        

    'ar' => [
        'title' => 'الاسم باللغة العربية',
        'address' => 'العنوان باللغة العربية',
        'description' => 'الوصف باللغة العربية',
        'termtitle' =>'العنوان باللغه العربية',
        'feattitle' =>'الميزة باللغه العربية',
        'city' =>'المدينة باللغة العربية',
        'service' =>'الخدمة باللغة العربية',
        'helping' =>'المساعدة باللغة العربية',
        'terming' =>' الشروط والأحكام بالعربية',
        'propaganding' =>'شروط الاعلان بالعربية',
        'service_description' =>'وصف الخدمة بالعربية',
        'aboutus' =>'من نحن بالعربية',

    ],

    'en' => [
        'title' => 'الاسم باللغة الانجليزية',
        'address' => 'العنوان باللغة الانجليزية',
        'description' => 'الوصف باللغة الانجليزية',
        'termtitle' =>'العنوان باللغه الانجليزية',
        'feattitle' =>'الميزة باللغه الانجليزية',
        'city' =>'المدينه باللغة الانجليزية',
        'service' =>'الخدمة باللغة الانجليزية',
        'helping' =>'المساعدة باللغة الانجليزية',
        'terming' =>' الشروط والأحكام بالانكليزية',
        'propaganding' =>'شروط الاعلان بالانكليزية',
        'service_description' =>'وصف الخدمة بالانجليزية',
        'aboutus' =>'من نحن بالانجليزية',


    ]
    
];