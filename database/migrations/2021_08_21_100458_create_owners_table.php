<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->default('00000');
            $table->string('commerical')->default('00000');
            
            $table->string('bank_name');
            $table->string('bank_account_owner');
            $table->string('account_number');
            
            $table->unsignedBigInteger('service_id')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();

            $table->integer('code')->nullable();
            $table->enum('type',['company'])->default('company');

            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->mediumText('address');
            $table->boolean('isVerified')->default(false);
            $table->string('min_salary',9,2);
            $table->string('image')->default('default.png');
            $table->string('image_owner')->default('default.png');
            $table->string('location')->nullable();
            $table->boolean('active')->default(true);
            $table->string('google_token')->nullable();

            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
