<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->unsignedBigInteger('service_id')->nullable();

            $table->enum('action',['wait','accept','finished'])->default('wait');
            $table->mediumText('refuse_reason')->nullable();

            $table->integer('price');
            $table->integer('quantity');
            $table->integer('total_price');

            $table->string('lat')->default('00.0000000');
            $table->string('lang')->default('00.0000000');
            $table->text('address')->nullable();
            $table->text('note')->nullable();

            $table->string('res_time')->nullable();
            $table->string('res_date')->nullable();

            $table->string('image')->default('default.png');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
