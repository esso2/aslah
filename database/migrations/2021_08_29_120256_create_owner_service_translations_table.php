<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerServiceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_service_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_services_id');
            $table->string('locale')->index();

            

            $table->unique(['owner_services_id', 'locale']);
            $table->foreign('owner_services_id')->references('id')->on('owner_services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_service_translations');
    }
}
