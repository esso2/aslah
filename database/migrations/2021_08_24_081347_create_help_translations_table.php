<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHelpTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('help_id');
            $table->string('locale')->index();
            $table->string('question');

            $table->unique(['help_id', 'locale']);
            $table->foreign('help_id')->references('id')->on('helps')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_translations');
    }
}
