<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutusTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aboutus_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aboutus_id');
            $table->string('locale')->index();
            $table->longText('aboutuses');

            $table->unique(['aboutus_id', 'locale']);
            $table->foreign('aboutus_id')->references('id')->on('aboutuses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aboutus_translations');
    }
}
