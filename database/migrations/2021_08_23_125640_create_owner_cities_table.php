<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_cities', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('owner_id')->nullable();

            $table->string('lat')->default('00.0000000');
            $table->string('lang')->default('00.0000000');
            $table->text('address')->index();

            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('owner_id')->references('id')->on('owners')->onUpdate('cascade')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_cities');
    }
}
