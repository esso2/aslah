<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_address_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_address_id');
            $table->string('locale')->index();
            $table->text('address')->index();
            $table->string('nameaddress')->nullable();

            $table->unique(['user_address_id', 'locale']);
            $table->foreign('user_address_id')->references('id')->on('user_addresses')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_address_translations');
    }
}
