<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropagandaTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propaganda_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('propaganda_id');
            $table->string('locale')->index();
            $table->longText('propagandas');

            $table->unique(['propaganda_id', 'locale']);
            $table->foreign('propaganda_id')->references('id')->on('propagandas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propaganda_translations');
    }
}
