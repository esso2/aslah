<?php

namespace Database\Seeders;

use App\Models\Term;
use Illuminate\Database\Seeder;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Term::create([
            'ar' => [
                'terms'       => 'الشروط والاحكام الشروط والاحكام الشروط والاحكام الشروط والاحكام ',
            ],
            'en' => [
                'terms'       => 'terms terms terms terms terms terms terms ',
            ],
        ]);
    }
}
