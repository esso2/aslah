<?php

namespace Database\Seeders;

use App\Models\Help;
use Illuminate\Database\Seeder;

class HelpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Help::create([
            'support_number'=>'00000000',
            'vedio_link'=>'https://www.youtube.com/watch?v=23ruEfLScnM',
            'ar' => [
                'question'       => 'المــســـاعــدة',
            ],
            'en' => [
                'question'       => 'Helping Helping ',
            ],
        ]);
    }
}
