<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(AdminSeeder::class);
       // $this->call(SettingTableSeeder::class);
       $this->call(HelpSeeder::class);
       $this->call(AboutSeeder::class);
       $this->call(TermsSeeder::class);
       $this->call(PropagandaSeeder::class);
       $this->call(CompSeeder::class);
    }
}
