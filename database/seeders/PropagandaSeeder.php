<?php

namespace Database\Seeders;

use App\Models\Propaganda;
use Illuminate\Database\Seeder;

class PropagandaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Propaganda::create([
            'ar' => [
                'propagandas'       => 'الاعلانات الاعلانات الاعلانات الاعلانات تفاصيل الاعلانات  ',
            ],
            'en' => [
                'propagandas'       => 'propagandas propagandas propagandas propagandas propagandas',
            ],
        ]);
    }
}
