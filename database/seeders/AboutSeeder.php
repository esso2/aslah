<?php

namespace Database\Seeders;

use App\Models\Aboutus;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Aboutus::create([
            'ar' => [
                'aboutuses'       => 'من نحن  من نحن من نحن ',
            ],
            'en' => [
                'aboutuses'       => 'Aboutus Aboutus ',
            ],
        ]);
    }
}
